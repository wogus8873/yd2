package com.yedam.set;

public class Member {
	//필드
	public String name;
	public int age;
	
	//생성자
	public Member(String name,int age) {
		this.name= name;
		this.age = age;
	}

	@Override
	public int hashCode() {
		return name.hashCode() + age;
	}

	@Override
	public boolean equals(Object obj) {
		if(obj instanceof Member) {
			Member member = (Member)obj;
			return member.name.equals(this.name) && (member.age==this.age);
		}
		return false;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
