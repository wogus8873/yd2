package com.yedam.poly;

public class HanKookTire extends Tire {

	
	//필드
	
	//생성자
	public HanKookTire(String location, int maxRotation) {
		super(location, maxRotation);
		
	}
	//메소드
	@Override
	public boolean roll() {
		++accRotation;
		if(accRotation < maxRotation) {
			//현재수명이 남아있으면
			System.out.println(location + "Tire 수명 : "+ (maxRotation-accRotation)+"회");
			return true;
		}else {
			System.out.println("#########"+location+"Tire 펑크"+"#########");
			return false;
		}
	
	}
}