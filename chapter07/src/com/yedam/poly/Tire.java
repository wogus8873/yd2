package com.yedam.poly;

public class Tire {
	//필드
	public int maxRotation; //최대회전수 타이어수명
	public int accRotation; //누적회전수 현재까지의회전수
	public String location; //타이어 위치
	//생성자
	public Tire(String location, int maxRotation) {
		this.location = location;
		this.maxRotation = maxRotation;
		
	}
	//메소드
	//타이어가 돌아가는 메소드
	public boolean roll() {
		++accRotation;
		if(accRotation < maxRotation) {
			//현재수명이 남아있으면
			System.out.println(location + "Tire 수명 : "+ (maxRotation-accRotation)+"회");
			return true;
		}else {
			System.out.println("#########"+location+"Tire 펑크"+"#########");
			return false;
		}
	}
	
	
}
