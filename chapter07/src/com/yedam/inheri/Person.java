package com.yedam.inheri;

public class Person extends People {
	
	//내가쓴것
	//여태까지는 기본생성자로 생성해서  생성자를 따로 정의 안 했지만
	//People에서는 생성자를 정의했음 
	//자식인 person에서는 super를 써서 가져온다고 표기 해야함
	public int age;
	//고수님이쓴것
	//자식객체를 만들때 , 생성자를 통해 만든다
	//super() 를 통해서 부모 객체를 생성한다 
	//여기서 super()가 의미하는것은 부모생성자를 호출하는것
	//따라서 자식객체를 만들게 되면 부모객체도 같이 만들어진다
	public Person(String name , String ssn) {
		super(name,ssn);
	}
}
