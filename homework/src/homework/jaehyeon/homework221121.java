package homework.jaehyeon;

public class homework221121 {
	public static void main(String[] args) {
		int A = 37;
		int B = 91;
		//문제 2
		System.out.println(B+A);
		System.out.println(B-A);
		System.out.println(B*A);
		System.out.println(B/A);
		//문제 3
		int var1 = 128;
		System.out.printf("128,int");//3-1
		String var2 = "B";
		System.out.printf("B,String");//3-2
		int var3 = 44032;
		System.out.printf("44032,int");//3-3
		long var4 = 100000000000L;
		System.out.printf("100000000000L,long");//3-4
		double var5 = 43.93106;
		System.out.printf("43.93106,double");//3-5
		double var6 = 301.3;
		System.out.printf("301.3,double");
		
		//문제 4
		byte a = 35;
		byte b = 25;
		int c = 463;
		long d = 1000L;
		
		int result1; 
		result1 = a + b + c;
		System.out.println(result1);//4-1
		
		long result2;
		result2 = a + c + d;//4-2
		
		double e = 45.31;
		double result = a + b + e;
		System.out.println(result);//4-3
		
		//문제 5
		
		/* 문제5) 아래와 같이 변수가 초기화되어있을 경우 다음과 같이 명시된대로 출력하세요.
	    출력예시) A278번지10.0 */
		int intValue1 = 24;
		int intValue2 = 3;
		int intValue3 = 8;
		float intValue4 = 10;
		char charValue = 'A';
		String strValue = "번지";
//		System.out.println(charValue+(intValue1+intValue2)+intValue3 );
		System.out.printf("%c%d%d%s%2.1f",charValue,intValue1+intValue2,intValue3,strValue,intValue4);
		
	}
}
