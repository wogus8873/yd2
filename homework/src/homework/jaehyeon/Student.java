package homework.jaehyeon;


public class Student {
	//setter , getter 쓴다고하면 필드를 프라이빗 설정하는게 거의 자동 
	//setter통한 필드 초기화
	//getter 통한 데이터 reading
	//getter 통한 학생정보 출력
	
	//필드
		private String name;
		private String dap;
		private int grade;
		private int programSco;
		private int dbSco;
		private int osSco;
	//생성자
//		public Student(){
//			
//		}
		//클래스를 통한 객체를 생성할때 첫번째로 수행하는 일들을 모아두는곳.
		//필드에 대한 데이터를 객체를 생성할 때 초기화 할 예정이라면
		//생성자에서 this 키워드를 통해 필드 초기화 하면 됨.
		
	//메소드
		
//		void setStu(String name , String dap, int grade , int programSco , int dbSco , int osSco ) {
//			this.name = name;
//			this.dap = dap;
//			this.grade = grade;
//			this.programSco = programSco;
//			this.dbSco = dbSco;
//			this.osSco = osSco;
//		}
		

		public String getName() {
			return name;
		}
		public void setName(String name) {
			this.name = name;
		}
		public String getDap() {
			return dap;
		}
		public void setDap(String dap) {
			this.dap = dap;
		}
		public int getGrade() {
			return grade;
		}
		public void setGrade(int grade) {
			this.grade = grade;
		}
		public int getProgramSco() {
			return programSco;
		}
		//프로그래밍 언어 점수가 0보다 적은수가 들어오면 프로그래밍 언어 점수 0 으로 처리
		public void setProgramSco(int programSco) {
			if(programSco <= 0 ) {
			this.programSco = 0;
			}
			this.programSco = programSco;
		}
		public int getDbSco() {
			return dbSco;
		}
		public void setDbSco(int dbSco) {
			this.dbSco = dbSco;
		}
		public int getOsSco() {
			return osSco;
		}
		public void setOsSco(int osSco) {
			this.osSco = osSco;
		}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
