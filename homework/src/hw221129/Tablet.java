package hw221129;

public interface Tablet extends Notebook {
		//상수필드
		public int TABLET_MODE = 2;
		
		//메소드
		public void watchVideo();
		public void useApp(); 
}
