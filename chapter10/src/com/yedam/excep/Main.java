package com.yedam.excep;

public class Main {
	public static void main(String[] args) {
		
		//nullPointException 
		//객체에 정보가 들어가지 안았을때 
		
//		String data = null;
//		System.out.println(data.toString());

//		Example example = null;
//		System.out.println(example.toString());
		
		//ArrayIndexOutOfBoundsException
		//범위를 벗어난 인덱스를 호출할 때
//		int[] intAry = new int[3];  //0~2
//		intAry[0] = 1;
//		intAry[1] = 2;
//		intAry[2] = 3;
		
//		intAry[3] = 3;      // 0~2 인데 "3"에 값을 준다고 해서 오류
		
		
/////////////////////////////////////////////////////////////////////////////////////////////////	
		
		
		//NumberFormatException
		//숫자로 바꿀 수 없는 문자를 숫자로 바꾸려고 할 때 
		
//		String str = "123";
//		int val = Integer.parseInt(str);
//		
//		System.out.println(val);
		
//		String str2 = "자바";
//		int val2 =  Integer.parseInt(str2);  //자바는 숫자로 바꿀 수 없음 그래서 오류
//		System.out.println(val2);
		
//		String str3 = "";
//		val = Integer.parseInt(str3);  // 공백도 숫자로 변환 할 수 없음 그래서 오류
//		System.out.println(val);
		
		
//////////////////////////////////////////////////////////////////////////////////////////////////
		
		//ClassCastException
		//자동타입변환 된 객체를 강제타입변환할 때 발생 예외
		
		//자동타입변환
		Example exam = new Exam();
		//강제타입변환
		Exam exam2 = (Exam)exam;
		
		//예외 케이스 -> instanceof 로 예방할 수 있음
		
		//Example 자신의 객체를 만듦
		Example exam3 = new Example();   
		//자신의 겍체를 자식으로 바꾸는것.
//		Exam exam4 = (Exam)exam3;   //자동타입변환으로 바꾸지 않았는데 강제타입변환 해서 그런것. 오류.
		
		//예외처리  
		//exam3 과 Exam이 같은 객체인지 확인하고 
		//강제타입변환
		if(exam3 instanceof Exam) {
			Exam exam5 = (Exam)exam3;
		}
		//ClassNotFoundExcoption - Class 파일 찾지 못 할때(실행메소드 못 찾을때 자주 발생)
		//ArithmeticException - 숫자를 0으로 나눌때
		//OutOfMemoryError - 메모리가 부족할 때(반복문의 종료를 하지 못하고 무한 루프때 간혹 발생)
		//IOException - 입출력 오류
		//FileNotFoundException - 해당 경로 파일 찾지 못할 때
		
		
		
		
		
	}
}
