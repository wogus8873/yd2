package com.yedam.student;

import java.util.ArrayList;
import java.util.List;

import com.yedam.common.DAO;

public class StudentDAO extends DAO{
	////////////////////////////////////////////////////////////
	private static StudentDAO studentDao = null;
	
	private StudentDAO() {
		
	}
	
	public static StudentDAO getInstence() {
		if(studentDao == null) {
			studentDao = new StudentDAO();
		}
		return studentDao;
	}
	/////////////////////////////////////////////////////////////
	//학생정보전체조회(전체 성적별 순서)
	//order by 학점 으로 ㄱ
	public List<Student> getStudentList(){
		List<Student> list = new ArrayList<>();
		Student student = null;
		try {
			conn();                              //이거 추가해서 성적별 순서
			String sql = "select * from student order by std_point";
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			
			while(rs.next()) {
				student = new Student();
				student.setStdId(rs.getInt("std_id"));
				student.setStdName(rs.getString("std_name"));
				student.setStdMahor(rs.getString("std_major"));
				student.setStdPoint(rs.getInt("std_point"));
				
				list.add(student);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconn();
		}
		return list;
	}
	//학생등록
	public int insertStudent(Student student) {
		int result = 0;
		try {
			conn();
			String sql = "insert into student values (?,?,?,?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, student.getStdId() );
			pstmt.setString(2, student.getStdName());
			pstmt.setString(3, student.getStdMahor());
			pstmt.setDouble(4, student.getStdPoint());
			
			
			result = pstmt.executeUpdate();
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconn();
		}
		return result;
	}
	//학생퇴학
	public int deleteStudent(String name) {
		int result = 0;
		try {
			conn();
			String sql = "delete from student where std_name =?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1,name);
			
			result = pstmt.executeUpdate();
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconn();
		}
		return result;
	}
	//학생정보수정(전공)
	public int updateStudent(Student student) {
		int result = 0;
		try {
			conn();
			String sql = "update student set std_major = ? where std_name = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1,student.getStdMahor());
			pstmt.setString(2,student.getStdName());
			
			result = pstmt.executeUpdate();
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconn();
		}
		
		
		return result;
	}

	
}
