package com.yedam.oop;

public class Student {
	//필드
	String stuName;
	String stuSch;
	int stuNum;
	
	int kor;
	int math;
	int eng;
	
	//생성자
	public Student() {
		
	}
	public Student(String stuName,String stuSch,int stuNum,int kor,int math,int eng) {
		this.stuName = stuName;
		this.stuSch = stuSch;
		this.stuNum = stuNum;
		this.kor = kor;
		this.math = math;
		this.eng = eng;
	}
	//메소드
	int sum() {
		return (kor+math+eng);	
	}
	double avg() {
		double avgs = sum() / (double)3;
		return avgs;
	}
	
	
	void getInfo() {
		System.out.println("학생의 이름 : "+stuName);
		System.out.println("학생의 학교 : "+stuSch);
		System.out.println("학생의 학번 : "+stuNum);
		System.out.println(sum());
		System.out.println(avg());
	}
	
	
}
