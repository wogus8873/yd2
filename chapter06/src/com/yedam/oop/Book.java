package com.yedam.oop;

public class Book {
	//필드
		String bookName;
		String bookType;
		int bookPrice;
		String bookPub;
		String isbn;
	//생성자
		public Book(String bookName,String bookType,int bookPrice,String bookPub,String isbn){
			this.bookName = bookName;
			this.bookType = bookType;
			this.bookPrice = bookPrice;
			this.bookPub = bookPub;
			this.isbn = isbn;
		}
	//메소드
//		위 내용을 출력할 수 있도록 Book 클래스를 생성하고 내용을 출력 할수 있도록
		void getInfo(){
			System.out.println("책이름 :"+bookName);
			System.out.println("책종류 :"+bookType);
			System.out.println("책가격 :"+bookPrice);
			System.out.println("출판사 :"+bookPub);
			System.out.println("도서번호 :"+isbn);
			
		}
		
		
		
		
}
