package com.bank.account;

//import java.util.Date;
import java.sql.Date;

import lombok.Data;

@Data
public class Account {
	/*ACCOUNT_ID NOT NULL VARCHAR2(20) 
	MEMBER_ID  NOT NULL VARCHAR2(20) 
	BALANCE             NUMBER       
	CREDATE             DATE
	*/
	
	private String accountId;
	private String memberId;
	private int balance;
	private Date credate;
	

	
	
}
