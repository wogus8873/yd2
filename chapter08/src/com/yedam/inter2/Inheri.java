package com.yedam.inter2;

public class Inheri {
	public static void main(String[] args) {
		//A<-B<-D(A<-D)
		//I  C  C
		
		//A<-B
		A a = new B();
		a.info();
		
		//A<-D
		A a2 = new D();
		a2.info();
		//여기서 D의 메소드를 지우면 B(부모)의 메소드가 출력됨 왜냐하면 상속관계라서
		
		
	
	}
}
