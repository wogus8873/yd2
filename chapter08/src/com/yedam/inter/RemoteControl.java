package com.yedam.inter;

public interface RemoteControl extends Searchable {
	//상수
	
	public static final int MAX_VOLUME = 10;  //static final생략가능 없어도 있음..
	public int MIN_VOLUME = 0;
	
	
	//추상메소드
	
	public void turnOn();
	public abstract void turnOff();  //abstract 도 생략 가능
	public void setVolume(int volume);
	
	//자식(구현클래스) 는 위의 메소드 세개는 무조건 만들어(오버라이딩)야함 
	
	
	
}
