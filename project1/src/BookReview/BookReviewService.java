package BookReview;

import java.util.List;
import java.util.Scanner;

import com.yedam.book.Book;
import com.yedam.mybook.MyBook;
import com.yedam.mybook.MyBookDAO;

public class BookReviewService {
	Scanner sc = new Scanner(System.in);
	

	//review 별점
	public void score() {
//		BookReview br = new BookReview();
		MyBook mb = new MyBook();
		Book book = new Book();
		int bId = 0;
		int score = 0;
		String txt = "";
		
		System.out.println("review 할 도서번호를 입력하세요");
		bId = Integer.parseInt(sc.nextLine());
		
		mb.setbId(bId);
		 mb = BookReviewDAO.getInstance().duplicated(mb);
		
		if(mb != null) {
			
			while(true) {
				System.out.println("점수를 입력해주세요 (1~5)");
				System.out.println("==============================");
				score = Integer.parseInt(sc.nextLine());
				if(score >= 1 && score < 6) {
					book.setbId(bId);
					
					System.out.println("review 를 작성해주세요 : ");
					txt = sc.nextLine();
					
					int result = BookReviewDAO.getInstance().score(book, score);
					int result2 = BookReviewDAO.getInstance().review(book, txt);
					
					if(result >= 1) {
						System.out.println("점수가 입력됐습니다.");
					}else if (result == 0) {
						System.out.println("입력 실패.");
					}
					
					if(result2 >= 1) {
						System.out.println("리뷰가 작성됐습니다.");
					}else if (result2 == 0) {
						System.out.println("리뷰작성 실패.");
					}
				}else{
					System.out.println("지정된 값이 아닙니다. 다시 입력하세요.(1~5)");
					System.out.println("==============================");
				}
				break;
			}
		}else {
			System.out.println("review는 책을 구매하거나 대여하는동안 작성가능합니다.");
		}
		
	}
	//리뷰조회
	public void getReview() {
		List<BookReview> list = BookReviewDAO.getInstance().getReview();
		
		for(BookReview br : list) {
			System.out.println("도서번호: "+br.getbId());
			System.out.println("리뷰: "+br.getReview());
			System.out.println("===================================");
		}
	
	}

	
	
	
	
	
	
	
	
	
	
}
