package BookReview;

import java.util.ArrayList;
import java.util.List;

import com.yedam.book.Book;
import com.yedam.common.DAO;
import com.yedam.mybook.MyBook;

public class BookReviewDAO extends DAO{
	private static BookReviewDAO bookReviewDao = null;
	private BookReviewDAO() {
	}
	public static BookReviewDAO getInstance() {
		if(bookReviewDao == null) {
			bookReviewDao = new BookReviewDAO();
		}
		return bookReviewDao;
	}
	//////////////////////////////////////////////////////////////////////////
	
	//리뷰 (별점)
	public int score(Book book ,int grade) {
		int result = 0;
		try {
			conn();
			String sql = "update book set b_grade = b_grade + ? where b_id = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, grade);
			pstmt.setInt(2, book.getbId());
			
			result = pstmt.executeUpdate();
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconn();
		}
		return result;
	}
	
	//mybook 소지 여부
	public MyBook duplicated(MyBook myBook) {
		try {
			conn();
			String sql = "select * from mybook where b_id = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1,myBook.getbId());
			rs = pstmt.executeQuery();
			
			//mybook에 있는 책이면
			if(rs.next()) {
				myBook = new MyBook();
				myBook.setbId(rs.getInt("b_id"));
				
			//mybook에 없는 책이면
			}else {
				myBook = null;
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconn();
		}
		return myBook;
	}
	
	//review insert
	public int review (Book book , String txt) {
		int result = 0;
		try {
			conn();
			String sql = "insert into book_review (b_id , review) \r\n"
					+ "values (?,?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, book.getbId());
			pstmt.setString(2, txt);
			
			result = pstmt.executeUpdate();
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconn();
		}
		return result;
	}
	
	//리뷰횟수
	public BookReview count(Book book) {
		BookReview br = null;
		try {
			conn();
			String sql = "select b_id , count(*)\r\n"
					+ "from book_review\r\n"
					+ "where b_id = ?\r\n"
					+ "group by b_id";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, book.getbId());
			
			rs = pstmt.executeQuery();
			
			if(rs.next()) {
				br = new BookReview();
				br.setbId(rs.getInt("b_id"));
				br.setCount(rs.getInt("count(*)"));
			}else {
				br = new BookReview();
//				br.setbId(rs.getInt("b_id"));
				br.setCount(1);
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconn();
		}
		return br;
	}
	

	//리뷰 보기
	public List<BookReview> getReview(){
		List<BookReview> list = new ArrayList<>();
		try {
			conn();
			String sql = "select * from book_review";
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			
			while(rs.next()) {
				BookReview br = new BookReview();
				br.setbId(rs.getInt("b_id"));
				br.setReview(rs.getString("review"));
				list.add(br);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconn();
		}
		return list;
	}
	
}
