package com.yedam.exe;

import java.util.Scanner;

import com.yedam.book.BookService;
import com.yedam.mybook.MyBook;
import com.yedam.mybook.MyBookService;

public class BookSearch {
	Scanner sc = new Scanner(System.in);
	BookService bs = new BookService();
	MyBookService mb = new MyBookService();
	public BookSearch() {
		run();
		
	}
	
	public void run() {
		boolean plug = true;
		
		while(plug) {
			System.out.println("========================================================================================");
			System.out.println("1.전체도서조회 | 2.장르별조회 | 3.제목검색 | 4.도서구매 | 5.도서리뷰 | 6.추천도서 | 7.오타,오역 | 8.돌아가기");
			System.out.println("========================================================================================");
			System.out.println("메뉴를 입력하세요");
			String menu = sc.nextLine();
			
			switch(menu) {
			case "1":
				bs.getBookList();
				break;
			case "2":
				bs.getBookGenre();
				break;
			case "3":
				bs.getBookName();
				break;
			case "4":
				bs.bookBuy();
				break;
			case "5":
				new review();
				break;
			case "6":
				mb.genreRecommend();
				break;
			case "7":
				new ErrorPage();
				break;
			case "8":
				plug = false;
				break;
			}
		}	
	}	
}
