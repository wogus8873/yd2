package com.yedam.exe;

import java.util.Scanner;

import com.yedam.error.ErrorService;


public class ErrorPage {
	Scanner sc = new Scanner(System.in);
	ErrorService es = new ErrorService();
	public ErrorPage() {
		run();
		
	}
	
	public void run() {
		boolean plug = true;
		
		while(plug) {
			System.out.println("========================================");
			System.out.println("1.오타,오역제보 | 2.제보된 오타,오역 | 3.돌아가기 ");
			System.out.println("========================================");
			System.out.println("메뉴를 입력하세요");
			String menu = sc.nextLine();
			
			switch(menu) {
			case "1":
				es.error();
				break;
			case "2":
				es.getError();
				break;
			case "3":
				plug = false;
				break;
			}
		}	
	}	
}
