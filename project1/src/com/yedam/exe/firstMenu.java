package com.yedam.exe;

import java.util.Scanner;

import com.yedam.mem.MemService;

public class firstMenu {
	Scanner sc = new Scanner(System.in);
	
	MemService memService = new MemService();
	
	
	public firstMenu() {
		run();
	}
	
	public void run() {
		boolean plug = true;
		
		while(plug) {
			System.out.println("=================================================");
			System.out.println("1.로그인 | 2.회원가입 | 3.회원조회 | 4.비밀번호조회 | 5.종료");
			System.out.println("=================================================");
			System.out.println("메뉴를 입력하세요");
			String menu = sc.nextLine();
			
			switch(menu) {
			case "1":
				memService.login();
				if(memService.memInfo != null) {
					new LoginMenu();
				}
				break;
			case "2":
				memService.insertMem();
				break;
			case "3":
				memService.getMem();
				break;
			case "4":
				memService.getPw();
				break;
			case "5":
				System.out.println("끝");
				plug = false;
				break;
			}
			
		}
		
	}
	
	
	
	
}
