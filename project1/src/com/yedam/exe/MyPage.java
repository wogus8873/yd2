package com.yedam.exe;

import java.util.Scanner;

import com.yedam.mem.MemService;
import com.yedam.mybook.MyBookService;

public class MyPage {
	Scanner sc = new Scanner(System.in);
	MyBookService mb = new MyBookService();
	MemService ms = new MemService();
	
	public MyPage() {
		run();
	}
	
	public void run() {
		boolean plug = true;
		
		while(plug) {
			System.out.println("======================================================");
			System.out.println("1.내 도서목록 | 2.회원정보수정 | 3.내 정보 | 4.결제 | 5.돌아가기");
			System.out.println("======================================================");
			System.out.println("메뉴를 입력하세요");
			String menu = sc.nextLine();
			
			switch(menu) {
			case "1":
				mb.deleteBook();
				mb.getMyBookList();
				break;
			case "2":
				new memUpdatePage();
				break;
			case "3":
				ms.getMyMem();
				break;
			case "4":
				mb.payMent();
				break;
			case "5":
				plug = false;
				break;
			}
		}	
	}	
	
	
	
	
	
	
}
