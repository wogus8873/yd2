package com.yedam.mem;

import java.util.ArrayList;
import java.util.List;

import com.yedam.common.DAO;

public class MemDAO extends DAO {
	private static MemDAO memDao = null;
	
	private MemDAO() {
		
	}
	
	public static MemDAO getInstance() {
		if (memDao == null) {
			memDao = new MemDAO();
		}
		return memDao;
	}
/////////////////////////////////////////////////////////////////////////////////
	
	
	//로그인
	public Mem login(Mem mem) {
		try {
			conn();
			String sql = "select * from mem where mem_id = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1,mem.getMemId());
			rs = pstmt.executeQuery();
			//아이디가 있으면
			if(rs.next()) {
				mem = new Mem();
				mem.setMemId(rs.getString("mem_id"));
				mem.setMemPw(rs.getString("mem_pw"));
				mem.setMemName(rs.getString("mem_name"));
				mem.setMemPhone(rs.getNString("mem_phone"));
				mem.setMemEmail(rs.getString("mem_email"));
				mem.setCvc(rs.getInt("cvc"));
				mem.setMemGrade(rs.getString("mem_grade"));
				mem.setMemCash(rs.getInt("mem_cash"));
				mem.setMemTotalCash(rs.getInt("mem_totalcash"));
			//아이디가 없으면
			}else {
				mem = null;
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconn();
		}
		return mem;
	}
	
	//회원조회(단건)
	public Mem getMem(String memId) {
		Mem mem = null;
		try {
			conn();
			String sql = "select * from mem where mem_id = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, memId);
			
			rs = pstmt.executeQuery();
			
			if(rs.next()) {
				mem = new Mem();
				mem.setMemId(rs.getString("mem_id"));
				mem.setMemPw(rs.getString("mem_pw"));
				mem.setMemName(rs.getString("mem_name"));
				mem.setMemPhone(rs.getNString("mem_phone"));
				mem.setMemEmail(rs.getString("mem_email"));
				mem.setCvc(rs.getInt("cvc"));
				mem.setMemGrade(rs.getString("mem_grade"));
				mem.setMemCash(rs.getInt("mem_cash"));
				mem.setMemTotalCash(rs.getInt("mem_totalcash"));
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconn();
		}
		return mem;
	}
	//비밀번호 조회
	public Mem getPw (Mem mem){
		try {
			conn();
			String sql = "select * from mem where mem_id = ? AND cvc = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, mem.getMemId());
			pstmt.setInt(2, mem.getCvc());
			rs = pstmt.executeQuery();
			
			while(rs.next()) {
				mem = new Mem();
				mem.setMemId(rs.getString("mem_id"));
				mem.setMemPw(rs.getString("mem_pw"));
				mem.setMemName(rs.getString("mem_name"));
				mem.setMemPhone(rs.getNString("mem_phone"));
				mem.setMemEmail(rs.getString("mem_email"));
				mem.setCvc(rs.getInt("cvc"));
				mem.setMemGrade(rs.getString("mem_grade"));
				mem.setMemCash(rs.getInt("mem_cash"));
				mem.setMemTotalCash(rs.getInt("mem_totalcash"));
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconn();
		}
		return mem;
	}
	//회원등록
	public int insertMem(Mem mem) {
		int result = 0;
		try {
			conn();
			String sql = "insert into mem (mem_id,mem_pw,mem_name,mem_phone,mem_email,cvc) \r\n"
					+ "values (?,?,?,?,?,?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, mem.getMemId());
			pstmt.setString(2, mem.getMemPw());
			pstmt.setString(3, mem.getMemName());
			pstmt.setString(4, mem.getMemPhone());
			pstmt.setString(5, mem.getMemEmail());
			pstmt.setInt(6, mem.getCvc());
			
			result = pstmt.executeUpdate();
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconn();
		}
		return result;
	}
	
	//비밀번호 변경 
	public int memUpdatePw(Mem memInfo) {
		int result = 0;
		try {
			conn();
			String sql = "update mem set mem_pw = ? where mem_id = ? ";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, memInfo.getMemPw());
			pstmt.setString(2,memInfo.getMemId());
			
			result = pstmt.executeUpdate();
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconn();
		}
		return result;
	}
	//이메일 변경
	public int memUpdateEm(Mem memInfo) {
		int result = 0;
		try {
			conn();
			String sql = "update mem set mem_email = ? where mem_id = ? ";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, memInfo.getMemEmail());
			pstmt.setString(2,memInfo.getMemId());
			
			result = pstmt.executeUpdate();
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconn();
		}
		return result;
	}
	
	//탈퇴
	public int memDelete(Mem memInfo) {
		int result=0;
		try {
			conn();
			String sql = "delete from mem where mem_id =?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, memInfo.getMemId());
			
			result = pstmt.executeUpdate();
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconn();
		}
		return result;
	}
	
	//승급전
	public int memGradeUp(Mem memInfo) {
		int result = 0;
		try {
			conn();
			String sql = "update mem set mem_grade = ? where mem_id = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, memInfo.getMemGrade());
			pstmt.setString(2, memInfo.getMemId());
			
			result = pstmt.executeUpdate();
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconn();
		}
		return result;
	}
	
}
