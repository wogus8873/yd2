package com.yedam.mem;

public class Mem {
//	mem_id varchar2(30) primary key,
//	mem_pw varchar2(30) not null,
//	mem_name varchar2(15) not null,
//	mem_phone varchar2(13),
//	mem_email varchar2(50) ,
//	mem_grade varchar2(10) default 'bronze',
//	mem_cash number(30) default 0
	private String memId;
	private String memPw;
	private String memName;
	private String memPhone;
	private String memEmail;
	private int cvc;
	private String memGrade;
	private double memCash;
	private int memTotalCash;
	public String getMemId() {
		return memId;
	}
	public void setMemId(String memId) {
		this.memId = memId;
	}
	public String getMemPw() {
		return memPw;
	}
	public void setMemPw(String memPw) {
		this.memPw = memPw;
	}
	public String getMemName() {
		return memName;
	}
	public void setMemName(String memName) {
		this.memName = memName;
	}
	public String getMemPhone() {
		return memPhone;
	}
	public void setMemPhone(String memPhone) {
		this.memPhone = memPhone;
	}
	public String getMemEmail() {
		return memEmail;
	}
	public void setMemEmail(String memEmail) {
		this.memEmail = memEmail;
	}
	public String getMemGrade() {
		return memGrade;
	}
	public void setMemGrade(String memGrade) {
		this.memGrade = memGrade;
	}
	public double getMemCash() {
		return memCash;
	}
	public void setMemCash(double d) {
		this.memCash = d;
	}
	public int getCvc() {
		return cvc;
	}
	public void setCvc(int cvc) {
		this.cvc = cvc;
	}
	public int getMemTotalCash() {
		return memTotalCash;
	}
	public void setMemTotalCash(int memTotalCash) {
		this.memTotalCash = memTotalCash;
	}
	
	
	
	
}
