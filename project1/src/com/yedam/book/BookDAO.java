package com.yedam.book;

import java.util.ArrayList;
import java.util.List;

import com.yedam.common.DAO;
import com.yedam.mem.MemService;
import com.yedam.mybook.MyBook;

public class BookDAO extends DAO{
	private static BookDAO bookDao = null;
	private BookDAO () {
		
	}
	public static BookDAO getInstance() {
		if(bookDao == null) {
			bookDao = new BookDAO();
		}
		return bookDao;
	}
//	private int bId;
//	private String bName;
//	private int bRPrice;
//	private int bKPrice;
//	private String bGenre;
//	private int bGrade;
//	private String bSummary;
//	private String bPub;
////////////////////////////////////////////////////////////////////////////////////////////
	
	public List<Book> getBookList(){
		List<Book> list = new ArrayList<>();
		Book book = null;
		
		try {
			conn();
			String sql = "select * from book";
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			
			while(rs.next()) {
				book = new Book();
				book.setbId(rs.getInt("b_id"));
				book.setbName(rs.getString("b_name"));
				book.setbRPrice(rs.getInt("b_r_price"));
				book.setbKPrice(rs.getInt("b_k_price"));
				book.setbGenre(rs.getString("b_genre"));
				book.setbGrade(rs.getInt("b_grade"));
				book.setbSummary(rs.getString("b_summary"));
				book.setbPub(rs.getString("b_publisher"));
				list.add(book);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconn();
		}
		return list;
	}
	
	//도서구매
	//랜탈
	public int bookRBuy(Book book) {
		int result = 0;
		try {
			conn();
			String sql = "select * from book where b_id = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, book.getbId());
			rs = pstmt.executeQuery();
			
			if(rs.next()) {
				book.setbId(rs.getInt("b_id"));
				book.setbName(rs.getString("b_name"));
				book.setbRPrice(rs.getInt("b_r_price"));
				book.setbKPrice(rs.getInt("b_k_price"));
				book.setbGenre(rs.getString("b_genre"));
				book.setbGrade(rs.getInt("b_grade"));
				book.setbSummary(rs.getString("b_summary"));
				book.setbPub(rs.getString("b_publisher"));
			}else {
				book = null;
			}
			
			String sql2 = "insert into mybook (b_id , b_name , b_genre , b_summary , b_publisher , mb_status)"
					+ "values (?,?,?,?,?,?)";
			pstmt = conn.prepareStatement(sql2);
			pstmt.setInt(1, book.getbId());
			pstmt.setString(2,book.getbName());
			pstmt.setString(3,book.getbGenre());
			pstmt.setString(4,book.getbSummary());
			pstmt.setString(5, book.getbPub());
			pstmt.setString(6,"R");
			
			result = pstmt.executeUpdate();
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconn();
		}
		return result;
	}
	//소장
	public int bookKBuy(Book book) {
		int result = 0;
		try {
			conn();
			String sql = "select * from book where b_id = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, book.getbId());
			rs = pstmt.executeQuery();
			
			if(rs.next()) {
				book.setbId(rs.getInt("b_id"));
				book.setbName(rs.getString("b_name"));
				book.setbRPrice(rs.getInt("b_r_price"));
				book.setbKPrice(rs.getInt("b_k_price"));
				book.setbGenre(rs.getString("b_genre"));
				book.setbGrade(rs.getInt("b_grade"));
				book.setbSummary(rs.getString("b_summary"));
				book.setbPub(rs.getString("b_publisher"));
			}else {
				book = null;
			}
			
			String sql2 = "insert into mybook (b_id , b_name , b_genre , b_summary , b_publisher , mb_status)"
					+ "values (?,?,?,?,?,?)";
			pstmt = conn.prepareStatement(sql2);
			pstmt.setInt(1, book.getbId());
			pstmt.setString(2,book.getbName());
			pstmt.setString(3,book.getbGenre());
			pstmt.setString(4,book.getbSummary());
			pstmt.setString(5, book.getbPub());
			pstmt.setString(6,"K");

			result = pstmt.executeUpdate();
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconn();
		}
		return result;
	}
	
	//cash 감소
	//랜탈
	public int rPriceUpdate(String id , Book book) {
		int result = 0;
		try {
			conn();
			
			String sql = "select * from book where b_id = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, book.getbId());
			rs = pstmt.executeQuery();
			
			if(rs.next()) {
				book.setbId(rs.getInt("b_id"));
				book.setbName(rs.getString("b_name"));
				book.setbRPrice(rs.getInt("b_r_price"));
				book.setbKPrice(rs.getInt("b_k_price"));
				book.setbGenre(rs.getString("b_genre"));
				book.setbGrade(rs.getInt("b_grade"));
				book.setbSummary(rs.getString("b_summary"));
				book.setbPub(rs.getString("b_publisher"));
			}else {
				book = null;
			}
			
			if(MemService.memInfo.getMemGrade().equals("bronze")) {
				MemService.memInfo.setMemCash(MemService.memInfo.getMemCash()-book.getbRPrice());
				String sql2 = "update mem set mem_cash = mem_cash - ? where mem_id = ?";
				pstmt = conn.prepareStatement(sql2);
				pstmt.setInt(1, book.getbRPrice());
				pstmt.setString(2, id);
			}else if(MemService.memInfo.getMemGrade().equals("silver")) {
				MemService.memInfo.setMemCash(MemService.memInfo.getMemCash()-(book.getbRPrice()-(book.getbRPrice()*0.05)));
				String sql2 = "update mem set mem_cash = mem_cash - (?-(?*0.05)) where mem_id = ?";
				pstmt = conn.prepareStatement(sql2);
				pstmt.setInt(1,book.getbRPrice());
				pstmt.setInt(2,book.getbRPrice());
				pstmt.setString(3, id);
			}else if(MemService.memInfo.getMemGrade().equals("gold")) {
				MemService.memInfo.setMemCash(MemService.memInfo.getMemCash()-(book.getbRPrice()-(book.getbRPrice()*0.1)));
				String sql2 = "update mem set mem_cash = mem_cash - (?-(?*0.1)) where mem_id = ?";
				pstmt = conn.prepareStatement(sql2);
				pstmt.setInt(1,book.getbRPrice());
				pstmt.setInt(2,book.getbRPrice());
				pstmt.setString(3, id);
			}
			
			result = pstmt.executeUpdate();
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconn();
		}
		return result;
	}
	//소장
	public int kPriceUpdate(String id , Book book) {
		int result = 0;
		try {
			conn();
			
			String sql = "select * from book where b_id = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, book.getbId());
			rs = pstmt.executeQuery();
			
			if(rs.next()) {
				book.setbId(rs.getInt("b_id"));
				book.setbName(rs.getString("b_name"));
				book.setbRPrice(rs.getInt("b_r_price"));
				book.setbKPrice(rs.getInt("b_k_price"));
				book.setbGenre(rs.getString("b_genre"));
				book.setbGrade(rs.getInt("b_grade"));
				book.setbSummary(rs.getString("b_summary"));
				book.setbPub(rs.getString("b_publisher"));
			}else {
				book = null;
			}
			
			if(MemService.memInfo.getMemGrade().equals("bronze")) {
				MemService.memInfo.setMemCash(MemService.memInfo.getMemCash()-book.getbKPrice());
				String sql2 = "update mem set mem_cash = mem_cash - ? where mem_id = ?";
				pstmt = conn.prepareStatement(sql2);
				pstmt.setInt(1, book.getbKPrice());
				pstmt.setString(2, id);
			}else if(MemService.memInfo.getMemGrade().equals("silver")) {
				MemService.memInfo.setMemCash(MemService.memInfo.getMemCash()-(book.getbKPrice()-(book.getbKPrice()*0.05)));
				String sql2 = "update mem set mem_cash = mem_cash - (?-(?*0.05)) where mem_id = ?";
				pstmt = conn.prepareStatement(sql2);
				pstmt.setInt(1,book.getbKPrice());
				pstmt.setInt(2,book.getbKPrice());
				pstmt.setString(3, id);
			}else if(MemService.memInfo.getMemGrade().equals("gold")) {
				MemService.memInfo.setMemCash(MemService.memInfo.getMemCash()-(book.getbKPrice()-(book.getbKPrice()*0.1)));
				String sql2 = "update mem set mem_cash = mem_cash - (?-(?*0.1)) where mem_id = ?";
				pstmt = conn.prepareStatement(sql2);
				pstmt.setInt(1,book.getbKPrice());
				pstmt.setInt(2, book.getbKPrice());
				pstmt.setString(3, id);
			}
				result = pstmt.executeUpdate();
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconn();
		}
		return result;
	}
	

	
	//mybook 소지 여부
	public MyBook duplicated(MyBook myBook) {
		try {
			conn();
			String sql = "select * from mybook where b_id = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1,myBook.getbId());
			rs = pstmt.executeQuery();
			
			//mybook에 있는 책이면
			if(rs.next()) {
				myBook = new MyBook();
				myBook.setbId(rs.getInt("b_id"));
				
			//mybook에 없는 책이면
			}else {
				myBook = null;
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconn();
		}
		return myBook;
	}
	
	
	
	
	
	
	
	
	
	
	
}
