<%@page import="java.util.List"%>
<%@page import="co.prod.member.vo.MemberVo"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%
		List<MemberVo> list = (List) request.getAttribute("list");
	%>
	<h3>회원 목록</h3>
	<table border = "1">
	<thead>
		<tr>
			<th>아이디</th><th>비번</th><th>휴대폰</th><th>주소</th><th>등급</th>
		</tr>
	<%
		for(MemberVo vo : list){
	%>
	<tr>
		<td><a href = "searchMember.do?id=<%=vo.getMemberId() %>"><%=vo.getMemberId() %></a></td>
		<td><%=vo.getMemberPw() %></td>
		<td><%=vo.getMemberPhone() %></td>
		<td><%=vo.getMemberAddr() %></td>
		<td><%=vo.getMemberGrade() %></td>
	</tr>
	<%
		}
	%>
	</table>
</body>
</html>