<%@page import="co.prod.member.vo.MemberVo"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%
		MemberVo vo = (MemberVo) request.getAttribute("updateId");
	%>
	
	<h3>업데이트된 결과페이지</h3>
	<table border = "1">
		<tr>
			<td>아이디 : </td><td><%=vo.getMemberId() %></td>
		</tr>
		<tr>
			<td>변경된 비번 : </td><td><%=vo.getMemberPw() %></td>
		</tr>
		<tr>
			<td>변경된 휴대폰 : </td><td><%=vo.getMemberPhone() %></td>
		</tr>
		<tr>
			<td>변경된 주소 : </td><td><%=vo.getMemberAddr() %></td>
		</tr>
			
	</table>
	
</body>
</html>