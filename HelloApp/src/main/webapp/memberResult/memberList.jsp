<%@page import="co.prod.member.vo.MemberVo"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>회원목록 페이지</title>
</head>
<body>
	<h3>등록된 회원 목록</h3>
	<%
		MemberVo vo = (MemberVo) request.getAttribute("memberInfo");
	%>
	<ul>
		<li><%=vo.getMemberId()%>,<%=vo.getMemberPw() %></li> <!-- html영역 -->
	</ul>
</body>
</html>