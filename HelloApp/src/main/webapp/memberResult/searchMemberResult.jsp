<%@page import="co.prod.member.vo.MemberVo"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>searchMemberResult</title>
</head>
<body>
	<%
		MemberVo vo = (MemberVo) request.getAttribute("memberInfo");
	%>


	<h3>조회결과페이지</h3>
	<table border = "1">
		<tr>
			<th>아이디</th><td><%=vo.getMemberId() %></td>
		</tr>
		<tr>
			<th>비밀번호</th><td><%=vo.getMemberPw() %></td>
		</tr>
		<tr>
			<th>이미지</th><td><img width = "100px" src="upload/<%=vo.getImage()%>"></td>
		</tr>
	</table>
</body>
</html>