package co.prod.member.web;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;

import co.prod.member.service.MemberDAO;
import co.prod.member.vo.MemberVo;
import co.prod.common.control;

public class addMemberControl implements control {

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
		String savePath = request.getServletContext().getRealPath("/upload");
		System.out.println(savePath);
		MultipartRequest multi = new MultipartRequest(request,savePath,1024*1024*10,"utf-8",new DefaultFileRenamePolicy());
		String id = multi.getParameter("id");     
		String pw = multi.getParameter("pw");     
		String ph = multi.getParameter("phone");  
		String ad = multi.getParameter("address");
		String gr = multi.getParameter("grade");  
		String im = multi.getParameter("image");
		
		Enumeration<?> files = multi.getFileNames();
		
		while(files.hasMoreElements()) {
			String file = (String) files.nextElement();
			System.out.println(file);
			im = multi.getFilesystemName(file);
		}
		
		MemberVo vo = new MemberVo();
		vo.setMemberId(id);
		vo.setMemberPw(pw);
		vo.setMemberPhone(ph);
		vo.setMemberAddr(ad);
		vo.setMemberGrade(gr);
		vo.setImage(im);
		
		MemberDAO dao = MemberDAO.getInstance();
		dao.insertMember(vo);
		
		request.setAttribute("memberInfo",vo);
		return "memberResult/memberList.jsp";
	}

}
