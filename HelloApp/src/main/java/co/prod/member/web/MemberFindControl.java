package co.prod.member.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.prod.common.control;
import co.prod.member.service.MemberDAO;
import co.prod.member.vo.MemberVo;

public class MemberFindControl implements control {

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
		String id = request.getParameter("id");
		
		MemberDAO dao = MemberDAO.getInstance();
		MemberVo vo = dao.getMember(id);
		
		request.setAttribute("memberInfo", vo);
		return "memberResult/searchMemberResult.jsp";
	}

}
