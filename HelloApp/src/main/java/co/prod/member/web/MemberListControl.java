package co.prod.member.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.prod.member.service.MemberDAO;
import co.prod.common.control;
import co.prod.member.vo.MemberVo;

public class MemberListControl implements control {

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
		
		MemberDAO dao = MemberDAO.getInstance();
		
		List<MemberVo> list = dao.getMemberList();
		
		
		request.setAttribute("list", list);
		return "memberResult/memberListResult.jsp";
	}

}
