package co.prod.member.web;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.prod.common.control;

//서블릿의 생명주기(생성자, init(), service(), destroy()
//init 은 한번 service 는 실행되는동안ㄱ ㅖ속? 디스트로이는 끝냄
//*.do url호출 실행되는 서블릿.
public class FrontController extends HttpServlet{
	
	HashMap<String,control> map;
	
	public FrontController() {
		System.out.println("FrontController 생성자.");
		map = new HashMap<>();
	}
	
	@Override
	//두번째거 오버라이딩
	public void init(ServletConfig config) throws ServletException {
		System.out.println("init 메소드 호출");
		map.put("/memberListResult.do", new MemberListControl());
		map.put("/addMember.do", new addMemberControl());
		map.put("/deleteMember.do", new MemberDelControl());
		map.put("/updateMember.do", new MemberModControl());
		map.put("/searchMember.do", new MemberFindControl());
	}
	
	@Override
	//http 값 받는거 오버라이딩
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("serviec 메소드 호출");
		String uri = req.getRequestURI(); //http://localhost:8081//HelloApp/memberList.do
		String path = req.getContextPath(); //   /HelloApp 를 가져오는
		String page = uri.substring(path.length());
		System.out.println(page);
		
		control result = map.get(page);
		String forwardPage = result.execute(req, resp);
		
		RequestDispatcher rd = req.getRequestDispatcher(forwardPage);
		rd.forward(req, resp);
		
	}
	
	private void distroy() {
		System.out.println("distroy 메소드 호출");
	}
	
	
	
	
	
	
}
