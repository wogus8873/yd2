package co.prod.member.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.prod.member.service.MemberDAO;
import co.prod.member.vo.MemberVo;
import co.prod.common.control;

public class MemberModControl implements control {

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
		String id = request.getParameter("id");
		String pw = request.getParameter("pw");
		String ad = request.getParameter("addr");
		String ph = request.getParameter("phone");
		
		MemberVo vo = new MemberVo();
		vo.setMemberId(id);
		vo.setMemberPw(pw);
		vo.setMemberPhone(ph);
		vo.setMemberAddr(ad);
		
		MemberDAO dao = MemberDAO.getInstance();
		dao.updateMember(vo);
		
		request.setAttribute("updateId", vo);
		return "memberResult/updateMemberResult.jsp";
	}

}
