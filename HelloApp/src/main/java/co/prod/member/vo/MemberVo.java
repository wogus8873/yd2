package co.prod.member.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class MemberVo {
	/*
  	hr/hr member table 기반 작성
 	MEM_ID    NOT NULL VARCHAR2(20)  
	MEM_PW    NOT NULL VARCHAR2(30)  
	MEM_PHONE          VARCHAR2(13)  
	MEM_ADDR           VARCHAR2(100) 
	MEM_GRADE          VARCHAR2(3)
*/ 
private String memberId;
private String memberPw;
private String memberPhone;
private String memberAddr;
private String memberGrade;
private String image;
}
