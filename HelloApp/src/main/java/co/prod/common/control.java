package co.prod.common;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface control {
	public String execute(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException;
}
