package co.dev.member.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.dev.member.service.MemberDAO;
import co.dev.member.vo.Member;


@WebServlet({ "/getMemberServlet", "/getMember" })
public class getMemberServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
  
    public getMemberServlet() {
        super();
    }


	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html;charset=utf-8");
		String id = req.getParameter("id");
		MemberDAO dao = MemberDAO.getInstance();
		Member vo = dao.getMember(id);
		
		req.setAttribute("memberInfo", vo);
		req.getRequestDispatcher("memberResult/searchMemberResult.jsp").forward(req, resp);
		
		//서블릿에서 html생성해주는 이전기능 : 사용안
//		StringBuilder sb = new StringBuilder();
//		sb.append("<h3>회원정보</h3>");
//		sb.append("<table border = 1>");
//		sb.append("<tr><td>회원 아이디 : </td><td>"+vo.getMemberId()+"</td></tr>");
//		sb.append("<tr><td>회원 비밀번호 : </td><td>"+vo.getMemberPw()+"</td></tr>");
//		sb.append("<tr><td>회원 주소 : </td><td>"+vo.getMemberAddr()+"</td></tr>");
//		sb.append("<tr><td>회원 휴대폰 : </td><td>"+vo.getMemberPhone()+"</td></tr>");
//		sb.append("<tr><td>회원 등급 : </td><td>"+vo.getMemberGrade()+"</td></tr>");
//		sb.append("</table>");
//		
//		sb.append("<a href = 'index.jsp'>첫페이지</a>");
//		String result = sb.toString();
//		resp.getWriter().print(result);
		
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
