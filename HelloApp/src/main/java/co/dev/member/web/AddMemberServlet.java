package co.dev.member.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.dev.member.service.MemberDAO;
import co.dev.member.vo.Member;

@WebServlet("/addMemberServlet")
public class AddMemberServlet extends HttpServlet{
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		
		//이거 안 넣으면 밑에 h3넣은거 적용이 안돼서 그대로 출력됨
		resp.setContentType("text/html;charset=utf-8");
		
		String id = req.getParameter("id");
		String pw = req.getParameter("pw");
		String ph = req.getParameter("phone");
		String ad = req.getParameter("address");
		String gr = req.getParameter("grade");
		
		Member vo = new Member();
		vo.setMemberId(id);
		vo.setMemberPw(pw);
		vo.setMemberPhone(ph);
		vo.setMemberAddr(ad);
		vo.setMemberGrade(gr);
		
		MemberDAO dao = MemberDAO.getInstance();
		int cnt = dao.insertMember(vo);
		if(cnt != 0) {
			System.out.println("성공ㅋㅋ");
		}else {
			System.out.println("실패..");
		}
		List<Member> list = dao.getMemberList();
		//출력스트림?
		//저장누르고 이거 출력됨
//		String result = "<h3>Success</h3>";
//		resp.getWriter().print(result);
		
		//memberList 페이지로 넘어가겠습니다
//		resp.sendRedirect("memberList"); addMemberServlet -> memberList
		//회원등록 페이지 -> 회원목록페이지 (서블릿 페이지 데이터조회하고 처리결과 html
		
		//회원등록페이지 (데이터조회) ->회원목록 페이지
		// addMemberServlet -> 회원목록을 출력하는 jsp 페이지.
		req.setAttribute("userName","Hongkildong");
		req.setAttribute("memberList",list);
		req.getRequestDispatcher("memberResult/memberList.jsp").forward(req, resp);
		
		
	}
}
