package co.dev.member.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.dev.member.service.MemberDAO;
import co.dev.member.vo.Member;

@WebServlet({"/updateMemberServlet","/updateMember"})
public class updatmeMemberServlet extends HttpServlet {
	private void doget(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	
	}


protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	
	resp.setContentType("text/html;charset=utf-8");
	
	String id = req.getParameter("id");
	MemberDAO dao = MemberDAO.getInstance();
	Member mem = dao.getMember(id);
	
	String pw = req.getParameter("pw");
	String ph = req.getParameter("phone");
	String ad = req.getParameter("addr");
	
	mem.setMemberId(id);
	mem.setMemberPw(pw);
	mem.setMemberPhone(ph);
	mem.setMemberAddr(ad);
	
	dao.updateMember(mem);
	
	Member vo = dao.getMember(id);
	
	
	
	req.setAttribute("memberInfo", vo);
	req.getRequestDispatcher("memberResult/updateMemberResult.jsp").forward(req, resp);
	}
}