package co.dev.member.web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.dev.member.service.MemberDAO;

@WebServlet("/deleteMemberServlet")
public class deleteMemberServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public deleteMemberServlet() {
        super();
    }

	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
	}

	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html;charset=utf-8");
		
		String id = req.getParameter("id");
		MemberDAO dao = MemberDAO.getInstance();
		int result = dao.deleteMember(id);
		String answer ="";
		
		if(result >= 1) {
			answer = "삭제됐습니다";
		}else if(result ==0) {
			answer = "실패";
		}
		req.setAttribute("memberInfo", answer);
		req.getRequestDispatcher("memberResult/deleteMemberResult.jsp").forward(req, resp);
		
	}

}
