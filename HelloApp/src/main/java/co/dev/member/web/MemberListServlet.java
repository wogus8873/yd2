package co.dev.member.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.dev.member.service.MemberDAO;
import co.dev.member.vo.Member;

@WebServlet("/memberList")
public class MemberListServlet extends HttpServlet {
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html;charset=utf-8");
		String result = "";
		
		
		MemberDAO dao = MemberDAO.getInstance();
		List<Member> list = dao.getMemberList();
		StringBuilder sb = new StringBuilder();
		sb.append("<h3>회원목록</h3>");
		sb.append("<table border = 1>");
		sb.append("<thead><tr><th>id</th><th>pw</th><th>addr</th><th>phone</th><th>grade</th></tr></thead>");
		sb.append("<tbody>");
		for (Member vo : list) {
			sb.append("<tr><td><a href = 'getMember?id="+vo.getMemberId()+"'>"+vo.getMemberId()+"</a></td><td>"+vo.getMemberPw()+"</td><td>"+vo.getMemberAddr()+"</td><td>"+vo.getMemberPhone()+"</td><td>"+vo.getMemberGrade()+"</td></tr>");
		}
		sb.append("</tbody>");
		sb.append("</table>");
		
		sb.append("<a href = 'index.jsp'>첫페이지</a>");
		result = sb.toString();
		resp.getWriter().print(result);
		
		
	}
}
