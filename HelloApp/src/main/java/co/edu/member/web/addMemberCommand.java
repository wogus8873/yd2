package co.edu.member.web;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.oreilly.servlet.MultipartRequest;
import com.oreilly.servlet.multipart.DefaultFileRenamePolicy;

import co.edu.member.service.MemberDAO;
import co.edu.common.Command;
import co.edu.member.vo.MemberVo;

public class addMemberCommand implements Command {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse resp ) throws ServletException, IOException {
		System.out.println("addMemberCommand 호출");
		//fileupload > multipart 인지 아닌지를 구분.
		//파라미터 : 1:요청정보, 2:저장위치 , 3:최대파일크기지정 , 4:인코딩 ,5:리네임정책
		String savePath = request.getServletContext().getRealPath("/upload");
		System.out.println(savePath);
		MultipartRequest multi = new MultipartRequest(request,savePath,1024*1024*10,"utf-8",new DefaultFileRenamePolicy());
		
		//multipart요청이면 request.getParameter 로는 값을 읽어오지 못하고
		//multipart request의 getParameter로 처리
		String id = multi.getParameter("id");     
		String pw = multi.getParameter("pw");     
		String ph = multi.getParameter("phone");  
		String ad = multi.getParameter("address");
		String gr = multi.getParameter("grade");  
		String im = multi.getParameter("image");
		
		//file객체. 
		Enumeration<?> files = multi.getFileNames();
		
		while(files.hasMoreElements()) {
			String file = (String) files.nextElement();
			System.out.println(file);
			im = multi.getFilesystemName(file);
		}
//		String id = request.getParameter("id");
//		String pw = request.getParameter("pw");
//		String ph = request.getParameter("phone");
//		String ad = request.getParameter("address");
//		String gr = request.getParameter("grade");
		
		MemberVo vo = new MemberVo();
		vo.setMemberId(id);
		vo.setMemberPw(pw);
		vo.setMemberPhone(ph);
		vo.setMemberAddr(ad);
		vo.setMemberGrade(gr);
		vo.setImage(im);
		
		MemberDAO dao = MemberDAO.getInstance();
		dao.insertMember(vo);
		
		request.setAttribute("memberInfo",vo);
		request.getRequestDispatcher("memberResult/memberList.jsp").forward(request, resp);
	}

}
