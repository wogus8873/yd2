package co.edu.member.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.edu.common.Command;
import co.edu.member.service.MemberDAO;

public class MemberDelCommand implements Command {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
		String id = request.getParameter("id");
		
		MemberDAO dao = MemberDAO.getInstance();
		dao.deleteMember(id);
		
		request.setAttribute("delId", id);
		request.getRequestDispatcher("memberResult/deleteMemberResult.jsp").forward(request, resp);
	}

}
