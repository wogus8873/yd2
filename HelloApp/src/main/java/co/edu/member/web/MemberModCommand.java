package co.edu.member.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.edu.common.Command;
import co.edu.member.service.MemberDAO;
import co.edu.member.vo.MemberVo;

public class MemberModCommand implements Command {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
		String id = request.getParameter("id");
		String pw = request.getParameter("pw");
		String ph = request.getParameter("phone");
		String ad = request.getParameter("addr");
		
		MemberVo memberVo = new MemberVo();
		memberVo.setMemberId(id);
		memberVo.setMemberPw(pw);
		memberVo.setMemberPhone(ph);
		memberVo.setMemberAddr(ad);
		
		MemberDAO dao = MemberDAO.getInstance();
		dao.updateMember(memberVo);
		
		
		request.setAttribute("updateId",memberVo );
		request.getRequestDispatcher("memberResult/updateMemberResult.jsp").forward(request, resp);

	}

}
