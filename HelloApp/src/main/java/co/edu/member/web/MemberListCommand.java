package co.edu.member.web;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.edu.common.Command;
import co.edu.member.service.MemberDAO;
import co.edu.member.vo.MemberVo;

public class MemberListCommand implements Command {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
		System.out.println("memberListCommand 호출.");
		
		MemberDAO dao = MemberDAO.getInstance();
		List<MemberVo> list = dao.getMemberList();
		
		request.setAttribute("memberList",list);
		request.getRequestDispatcher("memberResult/memberListResult.jsp").forward(request, resp);
	}

}
