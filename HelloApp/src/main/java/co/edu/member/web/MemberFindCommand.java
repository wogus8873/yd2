package co.edu.member.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.edu.common.Command;
import co.edu.member.service.MemberDAO;
import co.edu.member.vo.MemberVo;

public class MemberFindCommand implements Command {

	@Override
	public void execute(HttpServletRequest request, HttpServletResponse resp) throws ServletException, IOException {
		String id = request.getParameter("id");
		
		MemberDAO dao = MemberDAO.getInstance();
		MemberVo vo = dao.getMember(id);
		
		request.setAttribute("memberInfo", vo);
		request.getRequestDispatcher("memberResult/searchMemberResult.jsp").forward(request, resp);
		
	}

}
