package co.edu.member.service;

import java.util.ArrayList;
import java.util.List;

import co.dev.common.DAO;
import co.dev.member.vo.Member;
import co.edu.member.vo.MemberVo;

public class MemberDAO extends DAO{
	//로그인 , 회원 등록 , 회원 조회, 회원 탈퇴 ,  회원 수정 , 로그아웃
	//로그인 - login()
	//회원등록 - insertMember()
	//조회 - getMemberList()
	//탈퇴 - deleteMember()
	//수정 - updateMember()
	//로그아웃 - logout()
	
///////////////////////////////////////////////////////////////////   싱글톤
	private static MemberDAO memberDao = null;
	
	private MemberDAO() {
		
	}
	
	public static MemberDAO getInstance() {
		if(memberDao == null) {
			memberDao = new MemberDAO();
		}
		return memberDao;
	}
/////////////////////////////////////////////////////////////////////

	//1.로그인 id , pw 일치
	public Member login(Member member) {
		try {
			conn();
//			String sql = "select * from member where mem_id = ? AND mem_grade = n" 이렇게하면 관리자;
			String sql = "select * from member where mem_id = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, member.getMemberId());
			
			rs = pstmt.executeQuery();
			
			//id 는 primary key 라서 널이 있을 수 없다 따라서 아이디가 일치하지않으면 등록이 안 된것
			//정상적으로 회원정보 조회된 경우
			if(rs.next()) {
				member = new Member();
				member.setMemberId(rs.getString("mem_id"));
				member.setMemberPw(rs.getString("mem_pw"));
				member.setMemberPhone(rs.getString("mem_Phone"));
				member.setMemberAddr(rs.getString("mem_Addr"));
				member.setMemberGrade(rs.getString("mem_grade"));
			//회원의정보가 조회 안 된 경우
			}else {
				member = null;
				//조회할게없는데 객체만드는건 별로 그래서 null로 
			}
			
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconn();
		}
		return member;
	}
	
	
	
	//회원 조회
	public List<MemberVo> getMemberList(){
		List<MemberVo> list = new ArrayList<>();
		MemberVo member = null;
		
		try {
			conn();
			String sql = "select * from member";
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			
			while(rs.next()) {
				member = new MemberVo();
				member.setMemberId(rs.getString("member_id"));
				member.setMemberPw(rs.getString("member_pw"));
				member.setMemberPhone(rs.getString("member_phone"));
				member.setMemberAddr(rs.getString("member_addr"));
				member.setMemberGrade(rs.getString("member_grade"));
				list.add(member);
			}
			
			
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconn();
		}
		
		return list;
	}
	
	
	//회원등록
	public int insertMember(MemberVo Vo) {
		int result = 0;
		try {
			conn();
			String sql = "insert into member values (?,?,?,?,?,?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1,Vo.getMemberId());
			pstmt.setString(2, Vo.getMemberPw());
			pstmt.setString(3,Vo.getMemberPhone());
			pstmt.setString(4, Vo.getMemberAddr());
			pstmt.setString(5, Vo.getMemberGrade());
			pstmt.setString(6, Vo.getImage());
			
			result = pstmt.executeUpdate();
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconn();
		}
		
		
		
		return result;
	}
	
	//회원탈퇴
	public int deleteMember(String id) {
		int result = 0;
		try {
			conn();
			String sql = "delete from member where member_id = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1,id);
			
			result = pstmt.executeUpdate();
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconn();
		}
		return result;
	}
	
	//회원수정 -  비밀번호만수정
	public int updateMember(MemberVo memberVo) {
		int result = 0;
		try {
			conn();
			String sql = "update member set member_pw = ? ,member_addr = ? ,member_phone = ? where member_id = ?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setString(1, memberVo.getMemberPw());
			pstmt.setString(2, memberVo.getMemberAddr());
			pstmt.setString(3, memberVo.getMemberPhone());
			pstmt.setString(4, memberVo.getMemberId());
			
			result = pstmt.executeUpdate();
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconn();
		}
		
		
		return result;
		
	}
	
	
	public MemberVo getMember(String id) {
		MemberVo memberVo = null;
		try {
			conn();
			String sql = "select * from member where member_id ='"+id+"'";
			stmt = conn.createStatement();
			rs = stmt.executeQuery(sql);
			
			if(rs.next()) {
				memberVo = new MemberVo();
				memberVo.setMemberId(rs.getString("member_id"));
				memberVo.setMemberPw(rs.getString("member_pw"));
				memberVo.setMemberPhone(rs.getString("member_Phone"));
				memberVo.setMemberAddr(rs.getString("member_Addr"));
				memberVo.setMemberGrade(rs.getString("member_grade"));
				memberVo.setImage(rs.getString("image"));
			}
			
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconn();
		}
		return memberVo;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
