package com.yedam.quiz;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.yedam.common.DAO;
import com.yedam.student.Student;

public class QuizDAO extends DAO{
	
	private static QuizDAO quizeDao = null;
	private void QuizeDAO() {
		
	}
	public static QuizDAO getInstance() {
		if(quizeDao == null) {
			quizeDao = new QuizDAO();
		}
		return quizeDao;
	}
	
	
	//1.시험 문제 등록
	//2,조회 -> 과목별 조회만(응시과목출력)
	public List<Quiz> getQuizList(int subjectId){
		List<Quiz> list = new ArrayList<>();
		Quiz quiz = null;
		
		try {
			conn();
			String sql = "select * from quiz where subject_id =?";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1,subjectId);
			
			rs = pstmt.executeQuery();
			//과목별 시험지 조회
			while(rs.next()) {
				quiz = new Quiz();
				quiz.setSubjectId(rs.getInt("subject_id"));
				quiz.setExamQuest(rs.getString("Exam_quest"));
				quiz.setExamAnswer(rs.getString("exam_answer"));
				list.add(quiz);
				
			}
		}catch(Exception e){
			
		}finally {
			disconn();
		}
		return list;
	}
	
	//시험문제등록
	public void insertQuiz(Quiz quiz) {
		try {
			conn();
			String sql = "insert into quiz values (?,?,?)";
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1,quiz.getSubjectId());
			pstmt.setString(2,quiz.getExamQuest());
			pstmt.setString(3,quiz.getExamAnswer());
			
			if(pstmt.executeUpdate() == 1) {
				System.out.println("문제 입력 완료");
			}else {
				System.out.println("문제 입력 실패");
			}
		}catch(SQLException e) {
			System.out.println("error 에러코드 확인후 해결방안 확인");
			System.out.println("에러코드 : "+e.getErrorCode());
			System.out.println("사유    : "+e.getMessage());
		}finally {
			disconn();
		}
	}
	
	
	//성적입력 update
	public void updateScore(Student student) {
		try {
			conn();
			//성적이 국어일지 수학일지 영ㅇ어일지 어떻게 알지
			String sql = "";
			int score = 0;
			if(student.getStdKor() != -1) {
				sql = "update std set std_kor = ? where std_id = ?";
				//이프로 국어점수가 들어오면 쿼리를 실행할때 데이터를 넣어줘야함/ 국영수중 어느걸 넣을건지 지정해주려고 스코어 만든거 밑에 pstmt 물음표에
				score = student.getStdKor();
			}else if(student.getStdEng() != -1) {
				sql = "update std set std_eng = ? where std_id = ?";
				score = student.getStdEng();
			}else if(student.getStdMath() != -1) {
				sql = "update std set std_math = ? where std_id = ?";
				score = student.getStdMath();
			}
			pstmt = conn.prepareStatement(sql);
			pstmt.setInt(1, score);
			pstmt.setInt(2,student.getStdId());
			
			int result = pstmt.executeUpdate();
			
			if(pstmt.executeUpdate() == 1) {
				System.out.println("성적 입력 완료");
			}else {
				System.out.println("성적 입력 실패");
			}
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			disconn();
		}
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
