package com.yedam.quiz;

public class Quiz {

	private int subjectId; //1=국어 2=영어 3=수학
	private String examQuest;
	private String examAnswer;
	
	

	public int getSubjectId() {
		return subjectId;
	}
	public void setSubjectId(int subjectId) {
		this.subjectId = subjectId;
	}
	public String getExamQuest() {
		return examQuest;
	}
	public void setExamQuest(String examQuest) {
		this.examQuest = examQuest;
	}
	public String getExamAnswer() {
		return examAnswer;
	}
	public void setExamAnswer(String examAnswer) {
		this.examAnswer = examAnswer;
	}
	
	
	
}
