package com.yedam.student;

import java.util.List;
import java.util.Scanner;

public class StudentService {
	Scanner sc = new Scanner(System.in);
	
	//학생 전체조회 (전체)
	public void getStudentList() {
		List<Student> list = StudentDAO.getInstance().getStudentList();
		for(int i=0; i<list.size(); i++) {
			System.out.println("=======================================");
			System.out.println("학번 : "+list.get(i).getStdId());
			System.out.println("이름 : "+list.get(i).getStdName());
			System.out.println("교실 : "+list.get(i).getStdClass());
			System.out.println("국어 : "+list.get(i).getStdKor());
			System.out.println("영어 : "+list.get(i).getStdEng());
			System.out.println("수학 : "+list.get(i).getStdMath());
		}
		
	}
	//학생조회 (교실별)
	public void getStudentClass() {
		//어떤 교실을 조회할건지 입력받아야함
		int clazz = 0;
		System.out.println("조회할 교실 : ");
		clazz = Integer.parseInt(sc.nextLine());
		//학생정보조회
		List<Student> list = StudentDAO.getInstance().getStudentList();
		System.out.println("=================조회결과===================");
		//전체조회된 겨로가에서 입력한 교실만 출력
		for(Student std : list) {
			if(std.getStdClass() == clazz) {
				System.out.println("학번 : "+std.getStdId());
				System.out.println("이름 : "+std.getStdName());
				System.out.println("교실 : "+std.getStdClass());
				System.out.println("국어 : "+std.getStdKor());
				System.out.println("영어 : "+std.getStdEng());
				System.out.println("수학 : "+std.getStdMath());
				System.out.println("=======================================");
			}
		}
	}
	
	//성적확인  - 각 과목별 점수 ,  총점 ,  평균
	public void getStudentPoint() {
		//학생정보조회
		List<Student> list = StudentDAO.getInstance().getStudentList();
		System.out.println("=================성적확인===================");
		for(Student std : list) {
			int total = std.getStdKor() + std.getStdMath() + std.getStdMath(); 
			double avg = total / 3.0;
			System.out.println("학번 : "+std.getStdId());
			System.out.println("이름 : "+std.getStdName());
			System.out.println("교실 : "+std.getStdClass());
			System.out.println("국어 : "+std.getStdKor());
			System.out.println("영어 : "+std.getStdEng());
			System.out.println("수학 : "+std.getStdMath());
			System.out.println("총점 : "+total);
			System.out.println("평균 : "+avg);
		}
	}
	
	//학생조회 단건
	public void getStudent() {
		int stdId = 0;
		System.out.println("조회 학번 : ");
		stdId = Integer.parseInt(sc.nextLine());
		Student std= StudentDAO.getInstance().getStudent(stdId);
		//std 객체에 정보가 들어가있는지 확인
		if(std == null) {
			System.out.println("해당 학번은 조회되지 않습니다.");
		}else {
			System.out.println("=================조회결과===================");
			System.out.println("학번 : "+std.getStdId());
			System.out.println("이름 : "+std.getStdName());
			System.out.println("교실 : "+std.getStdClass());
			System.out.println("국어 : "+std.getStdKor());
			System.out.println("영어 : "+std.getStdEng());
			System.out.println("수학 : "+std.getStdMath());
		}
	}
	
	//시험응시유무 성적확인
	
	public void getStudentExamConfirm() {
		int stdId = 0;
		System.out.println("조회 학번 : ");
		stdId = Integer.parseInt(sc.nextLine());
		Student std= StudentDAO.getInstance().getStudent(stdId);
		//std 객체에 정보가 들어가있는지 확인
		if(std == null) {
			System.out.println("해당 학번은 조회되지 않습니다.");
		}else {
			System.out.println("=================조회결과===================");
			System.out.println("학번 : "+std.getStdId());
			//시험 응시 조건 : -1 이 아닐때, 시험점수는 음수값이 존재 할 수 없기때문에 
			//-1은 시험을 응시하지 않았다는것을 의미 ->학생등록할때 점수는 -1로 등록
			//입학할땐 시험점수가 없기때문
			if(std.getStdKor()==-1) {
				System.out.println("국어 안 쳤어요 ");
			}else {
				System.out.println("국어 : "+std.getStdKor());
			}
			
			if(std.getStdEng()==-1) {
				System.out.println("영어 안 쳤어요 ");
			}else {
				System.out.println("영어 : "+std.getStdEng());
			}
			
			if(std.getStdMath()==-1) {
				System.out.println("수학 안 쳤어요 ");
			}else {
				System.out.println("수학 : "+std.getStdMath());
			}
		}
	}
	//한명의 성적 확인
	public void getStudentPP() {
		System.out.println("조회할 학변: ");
		int stdId = Integer.parseInt(sc.nextLine());
		
		Student std = StudentDAO.getInstance().getStudent(stdId);
		if(std != null) {
			int total = std.getStdKor() + std.getStdMath() + std.getStdMath(); 
			double avg = total / 3.0;
			System.out.println("=================조회결과===================");
			System.out.println("학번 : "+std.getStdId());
			System.out.println("이름 : "+std.getStdName());
			System.out.println("교실 : "+std.getStdClass());
			System.out.println("국어 : "+std.getStdKor());
			System.out.println("영어 : "+std.getStdEng());
			System.out.println("수학 : "+std.getStdMath());
			System.out.println("총점 : "+total);
			System.out.println("평균 : "+avg);
		}else {
			System.out.println("해당 학번은 존재하지 않아요");
		}
	}
	
	
	//학생등록
	public void insertStudent() {
		//학생정보없는 객체 생성
 		Student std = new Student();
		System.out.println("학생정보 입력해 >");
		System.out.println("학번 : ");
		//학번:입학년도+월+교실+출결번호 (고려해보세요
		//학번을 규칙에 맞게 ? 그냥 넣은 데이터로?
		std.setStdId(Integer.parseInt(sc.nextLine()));
		System.out.println("이름 : ");
		std.setStdName(sc.nextLine());
		System.out.println("교실 : ");
		std.setStdClass(Integer.parseInt(sc.nextLine()));
		//시험응시유무 확인을 위해 디폴트값을 -1
		std.setStdKor(-1);
		std.setStdEng(-1);
		std.setStdMath(-1);
		
		StudentDAO.getInstance().insertStd(std);
		
	}
	//학생삭제
	public void deleteStudent() {
		System.out.println("학번 :");
		int stdId = Integer.parseInt(sc.nextLine());
		
		StudentDAO.getInstance().deleteStudent(stdId);
	}
	
	
	
	
	
}
