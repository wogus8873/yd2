package com.yedam.student;

import java.util.ArrayList;
import java.util.List;

import com.yedam.common.DAO;

public class StudentDAO extends DAO{
	//싱글톤
	private static StudentDAO stdDao = null;
	
	private StudentDAO () {
		
	}
	
	public static StudentDAO getInstance() {
		if(stdDao == null) {
			stdDao = new StudentDAO();
		}
		return stdDao;
	}
	
	//1.학생등록  삭제
	//2.학생조회 (전체 단건 교실별) + 5.시험 응시 확인 유무 + 3.성적확인
	//4.점수 입력 -> 시험치고나서 채점 바로하고 INSERT해주면 편하겠죠 (여기서 수정할거임 학생정보)
	//6.시험 응시 - quiz랑 조인 안 하고 code활용(quiz에서 code 조회해서 시험지 구성)
	//subject code ->subject = 1 이면 국어
	//               subject = 2 이면 영어               
	//               subject = 3 이면 수학
	//문서에 다 기입 혹은 코드주석 "무조건" 표기
	// 3 : 20 = X : 100
	//20X = 300
	//X = 300/20
	//X = 15
	
	//학생조회 전체(+교실벌+성적확인)
		public List<Student> getStudentList(){
			List<Student> list = new ArrayList<>();
			Student std = null;
			try {
				conn();
				String sql = "select * from std";
				stmt = conn.createStatement();
				rs = stmt.executeQuery(sql);
				
				while(rs.next()) {
					std = new Student();
					std.setStdId(rs.getInt("std_id"));
					std.setStdName(rs.getString("std_name"));
					std.setStdClass(rs.getInt("std_class"));
					std.setStdKor(rs.getInt("std_kor"));
					std.setStdEng(rs.getInt("std_eng"));
					std.setStdMath(rs.getInt("std_math"));
					
					list.add(std);
				}
			}catch(Exception e) {
				e.printStackTrace();
			}finally {
				disconn();
			}
			return list;
		}
	
	//학생조회 단건(+시험응시유무 확인)
		public Student getStudent(int stdId) {
			Student std = null;
			try {
				//db연결
				conn();
				//spl작성
				String sql = "select * from std where std_id = ?";
				//sql 실행준비
				pstmt = conn.prepareStatement(sql);
				//sql조건 데이터 입력
				pstmt.setInt(1, stdId);
				//sql실행 후 조회결과 가져오기 
				rs = pstmt.executeQuery();
				//조회된 결과가 존재한다면 std객체에 담아주기
				if(rs.next()) {
					std = new Student();
					std.setStdId(rs.getInt("std_id"));
					std.setStdName(rs.getString("std_name"));
					std.setStdClass(rs.getInt("std_class"));
					std.setStdKor(rs.getInt("std_kor"));
					std.setStdEng(rs.getInt("std_eng"));
					std.setStdMath(rs.getInt("std_math"));
				}
			}catch(Exception e) {
				e.printStackTrace();
			}finally {
				disconn();
			}
			return std;
		}
		
	//학생등록(다른방식으로 평소랑
		public void insertStd(Student student) {
			try {
				//db연결
				conn();
				//sql작성
				String sql = "insert into std values (?,?,?,?,?,?)";
				//sql실행준비
				pstmt = conn.prepareStatement(sql);
				//sql 데이터 입력
				pstmt.setInt(1, student.getStdId());
				pstmt.setString(2,student.getStdName());
				pstmt.setInt(3, student.getStdClass());
				pstmt.setInt(4,student.getStdKor());
				pstmt.setInt(5, student.getStdEng());
				pstmt.setInt(6, student.getStdMath());
				//sql실행(insert) 후 결과값 확인
				int result = pstmt.executeUpdate();
				//정상적으로 입력됐는지 안 됐는지 출력
				if(result >= 1) {
					System.out.println("정상수 입력");
				}else {
					System.out.println("입력 실패");
				}
			}catch(Exception e) {
				e.printStackTrace();
			}finally {
				disconn();
			}
		}
	//학생 삭제 
		public void deleteStudent(int std_id) {
			try {
				conn();
				String sql = "delete from std where std_id = ?";
				pstmt = conn.prepareStatement(sql);
				pstmt.setInt(1, std_id);
				
				int result = pstmt.executeUpdate();
				if(result >=1) {
					System.out.println("삭제 완료");
				}else {
					System.out.println("삭제 실패");
				}
				
			}catch(Exception e) {
				e.printStackTrace();
			}finally {
				disconn();
			}
		}
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
}
