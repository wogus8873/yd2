package com.coffee.exe;

import java.util.Scanner;

import com.coffee.logic.CoffeeService;

public class ExeApp {
	Scanner sc = new Scanner(System.in);
	int menuNo = 0;
	CoffeeService cs = new CoffeeService();
	
	
	public ExeApp() {
		start();
	}
	
	private void start() {
		while(menuNo != 7) {
			System.out.println("1.메뉴조회 | 2.메뉴상세조회 | 3.메뉴등록 | 4.판매 | 5.삭제 | 6.매출 | 7.종료");
			System.out.println("메뉴입력 : ");
			menuNo = Integer.parseInt(sc.nextLine());
			if(menuNo == 1) {
				cs.getCoffeeList();
			}else if(menuNo == 2) {
				cs.getCoffee();
			}else if(menuNo == 3) {
				cs.insertCoffee();
			}else if(menuNo == 4) {
				cs.salesCoffee();
			}else if(menuNo == 5) {
				cs.deleteMenu();
			}else if(menuNo == 6) {
				cs.storeSales();
			}else if(menuNo == 7) {
				System.out.println("종료");
			}else {
				System.out.println("1~7사이의 숫자만 입력하세요.");
			}
				
			
		}
	}
	
	
	
}
