/**
 * 
 */
//헤더 타이틀
let titles = ['아이디','이름','연락처','주소','삭제','수정'];
let columns = ['memberId','memberName','memberPhone','memberAddr'];
		
//member값을 던져주면 tr생성해주는 함수
function makeTr(member={}){
	let tr = document.createElement('tr');
	for(let column of columns){
		td = document.createElement('td');
		td.innerText = member[column];
		tr.append(td);
	}
	//삭제버튼
	td = document.createElement('td');
	let btn = document.createElement('button');
	btn.addEventListener('click',memberDeleteAjax);
	btn.innerText='삭제';
	td.append(btn);//<td><button>삭제</td ...
	tr.append(td);
	
	//수정버튼
	td = document.createElement('td');
	btn = document.createElement('button');
	btn.addEventListener('click',memberModifyForm);
	btn.innerText='수정';
	td.append(btn);//<td><button>수정</td ...
	tr.append(td);
	
	return tr; //함수 호출한 영역으로 값을 반환함
}




//멤버리스트 .
	//상수
	const xhtp = new XMLHttpRequest();
	xhtp.open('get','memberListAjaxJackson.do');
	xhtp.send();
	xhtp.onload = function(){
		console.log(xhtp);
		console.log(JSON.parse(xhtp.response));//parse:json -> javascrip.object   문자열이라 파싱해서 배열로 만듦
		let result = JSON.parse(xhtp.response);
		
		
		//table 생성
		let tbl = document.createElement('table');
		let thd = document.createElement('thead');
		let tbd = document.createElement('tbody');
		let tr = document.createElement('tr');
		
		//thead의 타이틀 지정.
		for(let title of titles){
			let th = document.createElement('th');
			th.innerText = title;
			tr.append(th);
			
		}
		thd.append(tr); //<thead><tr> ... </tr></thead>
		
		//tbody의 값을 지정
		for(let i=0; i<result.length; i++){
			let tr = makeTr(result[i])
			tbd.append(tr);
		}
		//테이블 자식으로 헤드 바디 넣음
		tbl.append(thd,tbd);
		console.log(tbl);
		
		//div요소의 하위에 tbl (table)지정
		tbl.setAttribute('border','1');
		tbl.className = 'table'; //<table class="table"> ...
		document.getElementById('show').append(tbl);
	}
	
	//등록버튼 클릭 이벤트.
	document.getElementById('addBtn').addEventListener('click',addMemberAjax);
	
	//등록버튼 Ajax호출
	function addMemberAjax(){
		let id = document.getElementById('mid').value;
		let pw = document.getElementById('mpw').value;
		let na = document.getElementById('mname').value;
		let ph = document.getElementById('mphone').value;
		let ad = document.getElementById('maddr').value;
		const member = {
			memberId: id,
			memberName: na,
			memberAddr: ad,
			memberPhone: ph,
			memberPw: pw	
		};
		const addAjax = new XMLHttpRequest();
		addAjax.open('post','addMemberAjax.do'); //post면 전송정보를 바디에 담아서 전송. get방식이면 헤드에 담아서 보냄 정보가 많으면 바디에 넣음
												 //헤드에 매핑해줘야한다고함 
		addAjax.setRequestHeader('Content-type','application/x-www-form-urlencoded');
		addAjax.send('id='+id+'&name='+na+'&addr='+ad+'&pass='+pw+'&phone='+ph);
		addAjax.onload = function(){
			console.log(addAjax.response);
			let result = JSON.parse(addAjax.response)
			if(result.retCode == 'Success'){
				alert('정상적으로 처리 완료');
				document.querySelector('#show tbody').append(makeTr(member));
			}else if(result.retCode == 'Fail'){
				alert('에러발생');
			}
		}
		
	}
	
	
	//삭제버튼 누르면 시작되는 이벤트 
	function memberDeleteAjax(){
		console.log(this.parentElement.parentElement.firstChild.innerText); 
		let user = this.parentElement.parentElement.firstChild.innerText;  //tr 지우려고 자식의 부모의 부모 
		let btn = this;
		//db에서 삭제. 화면에서 제외
		const xhtp = new XMLHttpRequest();
		xhtp.open('get','deleteMemberAjax.do?id='+user);
		xhtp.send();
		xhtp.onload = function(){
			console.log(xhtp.response);  //retCode = Success , retCode = Fail
			let result = JSON.parse(xhtp.response);
			if(result.retCode == 'Success'){
				alert('정상적으로 처리 완료');
				btn.parentElement.parentElement.remove();//화면에서 바로제거
			}else if(result.retCode == 'Fail'){
				alert('처리중 오류 발생');
			}
		}
	}
	
//수정화면
function memberModifyForm(){
	let tr = this.parentElement.parentElement;
	let mid = tr.children[0].textContent;
	let mname = tr.children[1].textContent;
	let mphone = tr.children[2].textContent;
	let maddr = tr.children[3].textContent;
	
	const member = {
			memberId: mid,
			memberName: mname,
			memberAddr: maddr,
			memberPhone: mphone
		};
	let newTr = document.createElement('tr');
	for(let column of columns){
		let td = document.createElement('td');
		if(column == 'memberAddr' || column == 'memberPhone'){
			let inp = document.createElement('input');
			inp.value = member[column];
			td.append(inp);
		}else {
			td.innerText = member[column];
		}
		newTr.append(td);
	}
		
	//삭제버튼
	td = document.createElement('td');
	let btn = document.createElement('button');
	btn.addEventListener('click',memberDeleteAjax);
	btn.innerText='삭제';
	btn.disabled = true;
	td.append(btn);//<td><button>삭제</td ...
	newTr.append(td);
	
	//수정버튼
	td = document.createElement('td');
	btn = document.createElement('button');
	btn.addEventListener('click',memberUpdateAjax);
	btn.innerText='변경';
	td.append(btn);//<td><button>수정</td ...
	newTr.append(td);
	
	//tr요소를 newTr요소로 대체	
	tr.replaceWith(newTr);
	
}
function memberUpdateAjax(){
	let tr = this.parentElement.parentElement;
	let id = tr.children[0].textContent;
	let ad = tr.children[3].firstChild.value;
	let ph = tr.children[2].firstChild.value;
	const modAjax = new XMLHttpRequest();
	
	modAjax.open('post','modMemberAjax.do'); //post면 전송정보를 바디에 담아서 전송. get방식이면 헤드에 담아서 보냄 정보가 많으면 바디에 넣음
											 //헤드에 매핑해줘야한다고함 
	modAjax.setRequestHeader('Content-type','application/x-www-form-urlencoded');
	modAjax.send('id='+id+'&addr='+ad+'&phone='+ph);
	modAjax.onload = function(){
		console.log(modAjax.response);
		let result = JSON.parse(modAjax.response)
		console.log(result);
		if(result.retCode == 'Success'){
			alert('정상적으로 처리 완료');
			let newTr = makeTr(result.memberInfo)
			tr.replaceWith(newTr);
			//document.querySelector('#show tbody').append(makeTr(member));
		}else if(result.retCode == 'Fail'){
			alert('에러발생');
		}
	}
}
	