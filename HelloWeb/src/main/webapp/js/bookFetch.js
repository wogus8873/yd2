/**
 * 
 */
let columns = ['BOOK_CODE','BOOK_TITLE','BOOK_AUTHOR','BOOK_PRESS','BOOK_PRICE'];
//member값을 던져주면 tr생성해주는 함수
function makeTr(book={}){
	console.log(book)
	let tr = document.createElement('tr');
	let td = document.createElement('td');
	let input = document.createElement('input');
	input.setAttribute("type", "checkbox")
	td.append(input)
	tr.append(td)
		for(let column of columns){
			td = document.createElement('td');
			td.innerText = book[column];
			tr.append(td);
			console.log(td)
		}
		
	//삭제버튼
	td = document.createElement('td');
	let btn = document.createElement('button');
	btn.addEventListener('click',bookDeleteAjax);
	btn.innerText='삭제';
	td.append(btn);//<td><button>삭제</td ...
	tr.append(td);
	console.log(tr)
	return tr; //함수 호출한 영역으로 값을 반환함
}


function showBookList(result){
	console.log(result);
	//table 생성
		let tbody = document.querySelector('#body')
		result.forEach(function(item,idx,ary){
			console.log(item);   //위에 makeTr의 매개값에 item 넣어서
			let tr = makeTr(item) 
			tbody.append(tr);
		})
}


function addBookFnc(){
	let bookCode = document.getElementById('bookCode').value;
	let bookTitle = document.getElementById('bookTitle').value;
	let bookAuthor = document.getElementById('bookAuthor').value;
	let bookPress = document.getElementById('bookPress').value;
	let bookPirce = document.getElementById('bookPrice').value;
	
	const obj = {
		BOOK_CODE: bookCode,
		BOOK_TITLE: bookTitle,
		BOOK_AUTHOR: bookAuthor,
		BOOK_PRESS: bookPress,
		BOOK_PRICE: bookPirce
	}
	
	const book = 'bookCode='+bookCode+'&bookTitle='+bookTitle+'&bookAuthor='+bookAuthor+'&bookPress='+bookPress+'&bookPrice='+bookPirce;
	
	//post 요청 . content-type 
	fetch("bookAddAjax.do",{
		method: 'post',
		headers: {'Content-Type':'application/x-www-form-urlencoded'},
		body: book
	})
	.then(resolve=>resolve.json())
	.then(result=>{
		if(result.retCode == 'Success'){
			makeTr(obj)
			alert('추가됨')
		}else if(result.retCode == 'Fail'){
			alert('처리중 에러.......')
		}
	})
	.catch(reject=>{
		console.log(reject)
	})
	
	
}




fetch('bookListAjax.do') 
.then(resolve=> resolve.json())
.then(showBookList)
.catch(function(reject){
	console.log(reject);
})

document.getElementById('addBtn').addEventListener('click',addBookFnc);
