/**
 * 
 */

//헤더 타이틀
let titles = ['아이디','이름','연락처','주소','삭제','수정'];
let columns = ['memberId','memberName','memberPhone','memberAddr'];
		
//member값을 던져주면 tr생성해주는 함수
function makeTr(member={}){
	let tr = document.createElement('tr');
	for(let column of columns){
		td = document.createElement('td');
		td.innerText = member[column];
		tr.append(td);
	}
	//삭제버튼
	td = document.createElement('td');
	let btn = document.createElement('button');
	btn.addEventListener('click',memberDeleteAjax);
	btn.innerText='삭제';
	td.append(btn);//<td><button>삭제</td ...
	tr.append(td);
	
	//수정버튼
	td = document.createElement('td');
	btn = document.createElement('button');
	btn.addEventListener('click',memberModifyForm);
	btn.innerText='수정';
	td.append(btn);//<td><button>수정</td ...
	tr.append(td);
	
	return tr; //함수 호출한 영역으로 값을 반환함
}

function showMemberList(result){
	console.log(result);
	//table 생성
		let tbl = document.createElement('table');
		let thd = document.createElement('thead');
		let tbd = document.createElement('tbody');
		let tr = document.createElement('tr');
		
		//thead의 타이틀 지정.
		for(let title of titles){
			let th = document.createElement('th');
			th.innerText = title;
			tr.append(th);
		}
		thd.append(tr); //<thead><tr> ... </tr></thead>
		
						 //멤버값하나,인덱스,멤버배열
	result.forEach(function(item,idx,ary){
		console.log(item);   //위에 makeTr의 매개값에 item 넣어서 
		tbd.append(makeTr(item));
	})
	//테이블 자식으로 헤드 바디 넣음
		tbl.append(thd,tbd);
		console.log(tbl);
		
		//div요소의 하위에 tbl (table)지정
		tbl.setAttribute('border','1');
		tbl.className = 'table'; //<table class="table"> ...
		document.getElementById('show').append(tbl);
	
}

function addMemberFnc(){
	let id = document.getElementById('mid').value;
	let pw = document.getElementById('mpw').value;
	let na = document.getElementById('mname').value;
	let ph = document.getElementById('mphone').value;
	let ad = document.getElementById('maddr').value;
	
	const obj = {
		memberId: id,
		memberName: na,
		memberAddr: ad,
		memberPhone: ph,
		memberPw: pw
	}
	
	const member = 'id='+id+'&name='+na+'&addr='+ad+'&pass='+pw+'&phone='+ph;
	
	//post 요청 . content-type 
	fetch("addMemberAjax.do",{
		method: 'post',
		headers: {'Content-Type':'application/x-www-form-urlencoded'},
		body: member
	})
	.then(resolve=>resolve.json())
	.then(result=>{
		if(result.retCode == 'Success'){
			makeTr(obj);
			alert('추가됨')
		}else if(result.retCode == 'Fail'){
			alert('처리중 에러.......')
		}
	})
	.catch(reject=>{
		console.log(reject)
	})
	
	
}

//목록출력. fetch api 활용
fetch('memberListAjaxJackson.do') //fetch('url').then(처리할함수).catch(오류처리할 함수)
.then(resolve=> resolve.json())//json stream 을 javascript object생성
	  //변수 받고=>반환해줄값
	 			//JSON.parse (파싱해준것) 과 같음
//위 then에서 값 받고 밑에 then에서 그 값으로 처리
.then(showMemberList)
.catch(function(reject){
	console.log(reject);
})

document.getElementById('addBtn').addEventListener('click',addMemberFnc);






