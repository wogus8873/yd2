/**
 * 
 */
//삭제버튼 누르면 시작되는 이벤트 
	function memberDeleteAjax(){
		console.log(this.parentElement.parentElement.firstChild.innerText); 
		let user = this.parentElement.parentElement.firstChild.innerText;  //tr 지우려고 자식의 부모의 부모 
		let btn = this;
		//db에서 삭제. 화면에서 제외
		fetch("deleteMemberAjax.do?id="+user,{
			method:'get'
		})
		.then(resolve=>resolve.json())
		.then(result=>{
			if(result.retCode == 'Success'){
				alert('정상적으로 처리 완료');
				btn.parentElement.parentElement.remove();//화면에서 바로제거
			}else if(result.retCode == 'Fail'){
				alert('처리중 오류 발생');
			}
		})
		.catch(reject=>{
			console.log(reject)
		})
			
		
	}
	
	function memberModifyForm(){
	let tr = this.parentElement.parentElement;
	let mid = tr.children[0].textContent;
	let mname = tr.children[1].textContent;
	let mphone = tr.children[2].textContent;
	let maddr = tr.children[3].textContent;
	
	const member = {
			memberId: mid,
			memberName: mname,
			memberAddr: maddr,
			memberPhone: mphone
		};
	let newTr = document.createElement('tr');
	for(let column of columns){
		let td = document.createElement('td');
		if(column == 'memberAddr' || column == 'memberPhone'){
			let inp = document.createElement('input');
			inp.value = member[column];
			td.append(inp);
		}else {
			td.innerText = member[column];
		}
		newTr.append(td);
	}
		
	//삭제버튼
	td = document.createElement('td');
	let btn = document.createElement('button');
	btn.addEventListener('click',memberDeleteAjax);
	btn.innerText='삭제';
	btn.disabled = true;
	td.append(btn);//<td><button>삭제</td ...
	newTr.append(td);
	
	//수정버튼
	td = document.createElement('td');
	btn = document.createElement('button');
	btn.addEventListener('click',memberUpdateAjax);
	btn.innerText='변경';
	td.append(btn);//<td><button>수정</td ...
	newTr.append(td);
	
	//tr요소를 newTr요소로 대체	
	tr.replaceWith(newTr);
	
}
function memberUpdateAjax(){
	let tr = this.parentElement.parentElement;
	let id = tr.children[0].textContent;
	let ad = tr.children[3].firstChild.value;
	let ph = tr.children[2].firstChild.value;
	
	fetch("modMemberAjax.do",{
		method: 'post',
		headers: {'Content-Type':'application/x-www-form-urlencoded'},		
		body: 'id='+id+'&addr='+ad+'&phone='+ph
	})
	.then(resolve=>resolve.json())
	.then(result=>{
		if(result.retCode == 'Success'){
			alert('정상적으로 처리 완료');
			let newTr = makeTr(result.memberInfo)
			
			tr.replaceWith(newTr);
		}else if(result.retCode == 'Fail'){
			alert('에러발생');
		}
	})
	.catch(reject=>{
		console.log(reject)
	})
		
		
	
}
	