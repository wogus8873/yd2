/*!
* Start Bootstrap - Shop Homepage v5.0.5 (https://startbootstrap.com/template/shop-homepage)
* Copyright 2013-2022 Start Bootstrap
* Licensed under MIT (https://github.com/StartBootstrap/startbootstrap-shop-homepage/blob/master/LICENSE)
*/
// This file is intentionally blank
// Use this file to add JavaScript to your project



fetch('../productList.do')
.then(resolve=>resolve.json())
.then(result=> {
	console.log(result)
	for(let i=0; i<result.length; i++){
		let template = document.querySelector('.col.mb-5').cloneNode(true);
		console.log(template.querySelector('h5'));//h5태그가 상품이름 감싼 태그라 h5불러옴 쿼리셀렉터로 

		
		//이름
		template.querySelector('h5').innerText = result[i].name;
		//이미지
		template.querySelector('img').setAttribute('src','../images/'+result[i].image)
		//a태그
		let aTag = document.createElement('a')
		aTag.setAttribute('href','item.html?code='+result[i].code)
		let imgTag = template.querySelector('img').cloneNode(true)
		aTag.append(imgTag)
		template.querySelector('img').replaceWith(aTag)
		//가격
		template.querySelector('span:nth-of-type(1)').innerText = result[i].orig;
		template.querySelector('span:nth-of-type(2)').innerText = result[i].sale;

		//
		let sItem = template.querySelector('div.d-flex')
		for(let j=0; j<5; j++){
			let star = document.createElement('div');
			if(j<Math.floor(result[i].like)){
				star.className = 'bi-star-fill'//한개
			}else if(j<result[i].like){
				star.className = 'bi-star-half'//반개
			}else{
				star.className = 'bi-star'//ㄴ
			}
			sItem.children[j].replaceWith(star)
		}
		document.querySelector('.col.mb-5').parentElement.append(template);
	}
	document.querySelector('.col.mb-5').style.display = 'none';
})
.catch(reject=>console.log(reject))

// let template = document.querySelector('.col.mb-5'); // div.col.mb-5
// let parentEl = template.parentElement; //div.row.gx-4.gx-lg-5 ...
// for (let i = 0; i < 5; i++){
// 	template = document.querySelector('.col.mb-5').cloneNode(true);
// 												  //앞에꺼를 그대로 복사해주겠습니다~~
// 												  //이걸 tmplate 에 담은것 
// 	parentEl.append(template);
// }