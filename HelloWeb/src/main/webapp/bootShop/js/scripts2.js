/**
 * 
 */
let url = location.href
let co = url.indexOf('code=')+5
let code = url.substring(co)
console.log(code)

fetch('../searchProduct.do',{
	method: 'post',
	headers: {'Content-Type':'application/x-www-form-urlencoded'},
	body: 'code='+code
})
.then(resolve=>resolve.json())
.then(result=> {
	console.log(result)
	
	let template = document.querySelector('section.py-5:nth-of-type(1)');
	//이름
	template.querySelector('h1').innerText = result.PROD_NAME;
	//설명
	template.querySelector('p').innerText = result.PROD_DESC;
	//이미지
	template.querySelector('img').setAttribute('src','../images/'+result.IMAGE);
	//가격
	
	template.querySelector('span:nth-of-type(1)').innerText = result.ORIG_PRICE;
	template.querySelector('span:nth-of-type(2)').innerText = result.SALE_PRICE;
})
.catch(reject=>console.log(reject))



fetch('../bestProduct.do')
.then(resolve=>resolve.json())
.then(result=>{
	
	for(let i=0; i<result.length; i++){
		let template = document.querySelector('.col.mb-5').cloneNode(true);
		console.log(template.querySelector('h5'));//h5태그가 상품이름 감싼 태그라 h5불러옴 쿼리셀렉터로 

		
		//이름
		template.querySelector('h5').innerText = result[i].PROD_NAME;
		//이미지
		template.querySelector('img').setAttribute('src','../images/'+result[i].IMAGE);
		console.log(template.querySelector('img'))
		
		//가격
		template.querySelector('span:nth-of-type(1)').innerText = result[i].ORIG_PRICE;
		template.querySelector('span:nth-of-type(2)').innerText = result[i].SALE_PRICE;

		//
		let sItem = template.querySelector('div.d-flex')
		for(let j=0; j<5; j++){
			let star = document.createElement('div');
			if(j<Math.floor(result[i].LIKE_IT)){
				star.className = 'bi-star-fill'//한개
			}else if(j<result[i].LIKE_IT){
				star.className = 'bi-star-half'//반개
			}else{
				star.className = 'bi-star'//ㄴ
			}
			sItem.children[j].replaceWith(star)
		}
		document.querySelector('.col.mb-5').parentElement.append(template);
	}
	document.querySelector('.col.mb-5').style.display = 'none';
	
})
.catch(reject=>console.log(reject))