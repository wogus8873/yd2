<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<h3>등록화면</h3>
<form action="insertNotice.do" method="post">
	<!-- 작성자 , 제목 , 내용 ,  -->
	<table class="table">
		<tr>
			<th>작성자</th><td><input type="text" name="writer"></td>
		</tr>
		<tr>
			<th>제목</th><td><input type="text" name="title"></td>
		</tr>
		<tr>
			<th>내용</th><td><input type="text" name="subject"></td>
		</tr>
		<c:choose>
			<c:when test="${not empty id }">
			<tr>
				<td colspan="2"><input type="submit" name="등록"></td>
			</tr>
			</c:when>
			<c:otherwise>
			<tr>
				<td colspan="2"><input disabled type="submit" name="등록"></td>
			</tr>
			</c:otherwise>
		</c:choose>
	</table>
</form>