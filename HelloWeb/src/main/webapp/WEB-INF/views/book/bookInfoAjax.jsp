<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<div class="container">
	<div class="center">
	
		<table class="table">
			<tr>
				<th>도서코드</th>
				<td><input type="text" id="bookCode"></td>
			</tr>
			<tr>
				<th>책이름</th>
				<td><input type="text" id="bookTitle"></td>
			</tr>
			<tr>
				<th>저자</th>
				<td><input type="text" id="bookAuthor"></td>
			</tr>
			<tr>
				<th>출판사</th>
				<td><input type="text" id="bookPress"></td>
			</tr>
			<tr>
				<th>가격</th>
				<td><input type="number" id="bookPrice"></td>
			</tr>
			<tr>
				<td colspan="2" align="center"><button class="btn btn-primary" id="addBtn">등록</button><button class="btn btn-primary" id="delBtn">선택삭제</button></td>
			</tr>
		</table>
		
		
		
		<div id="show">
			<table id="tb" class="table">
				<thead>
					<tr>
						<th><input type="checkbox" id="all" name="all"></th>
						<th>도서코드</th>
						<th>제목</th>
						<th>저자</th>
						<th>출판사</th>
						<th>가격</th>
						<th>삭제</th>
					</tr>
				</thead>
				
				<tbody id="body">
				
				</tbody>
			</table>
		</div>
		
	</div>
</div>

<script src="js/bookFunction.js"></script>
<script src="js/bookFetch.js"></script>