<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<form action="addMember.do" method="post">
	<!-- 작성자 , 제목 , 내용 ,  -->
	<table class="table">
		<tr>
			<th>아이디</th><td><input type="text" name="memberId"></td>
		</tr>
		<tr>
			<th>비밀번호</th><td><input type="password" name="memberPw"></td>
		</tr>
		<tr>
			<th>이름</th><td><input type="text" name="memberName"></td>
		</tr>
		<tr>
			<th>연락처</th><td><input type="text" name="memberPhone"></td>
		</tr>
		<tr>
			<th>주소</th><td><input type="text" name="memberAddr"></td>
		</tr>
		<tr>
			<td colspan="2"><input type="submit" value="등록"></td>
		</tr>
	</table>
</form>