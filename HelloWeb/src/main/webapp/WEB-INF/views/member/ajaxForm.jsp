<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="container">
	<div class="center">
		<h3>Ajax연습</h3>
		<table class="table">
			<tr>
				<th>아이디</th>
				<td><input type="text" id="mid"></td>
			</tr>
			<tr>
				<th>이름</th>
				<td><input type="text" id="mname"></td>
			</tr>
			<tr>
				<th>연락처</th>
				<td><input type="text" id="mphone"></td>
			</tr>
			<tr>
				<th>주소</th>
				<td><input type="text" id="maddr"></td>
			</tr>
			<tr>
				<th>비밀번호</th>
				<td><input type="password" id="mpw"></td>
			</tr>
			<tr>
				<td colspan="2" align="center"><button class="btn btn-primary" id="addBtn">등록</button></td>
			</tr>
		</table>
		
		<div id="show"></div>
		
	</div>
</div>



<!-- <script src="js/ajax.js"></script> -->
<script src="js/functions.js"></script>
<script src="js/ajaxFetch.js"></script>
