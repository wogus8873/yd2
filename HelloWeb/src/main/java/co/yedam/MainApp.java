package co.yedam;

import java.util.ArrayList;
import java.util.List;

import co.yedam.common.Criteria;
import co.yedam.common.PageDTO;
import co.yedam.notice.service.impl.NoticeServiceImplMybatis;
import co.yedam.notice.vo.noticeVo;

public class MainApp {
	public static void main(String[] args) {
		List<noticeVo> list = new ArrayList<>();
		noticeVo v1 = new noticeVo();
		v1.setNoticeId(100);
		noticeVo v2 = new noticeVo();
		v2.setNoticeId(200);
		noticeVo v3 = new noticeVo();
		v3.setNoticeId(300);
		
		list.add(v1);
		list.add(v2);
		list.add(v3);
		
		NoticeServiceImplMybatis mybatis = new NoticeServiceImplMybatis();
		for(noticeVo notice : mybatis.forEachTest(list)) {
			System.out.println(notice);
		}
	}
}
