package co.yedam.notice.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.yedam.common.Command;
import co.yedam.notice.service.NoticeService;
import co.yedam.notice.service.impl.NoticeServiceImpl;
import co.yedam.notice.service.impl.NoticeServiceImplMybatis;
import co.yedam.notice.vo.noticeVo;

public class InsertNotice implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {
		// TODO Auto-generated method stub
		//인서트 구현
		String writer = req.getParameter("writer");
		String title = req.getParameter("title");
		String subject = req.getParameter("subject");
		
		noticeVo vo = new noticeVo();
		vo.setNoticeWriter(writer);
		vo.setNoticeTitle(title);
		vo.setNoticeSubject(subject);
		
		NoticeService service = new NoticeServiceImplMybatis();
		service.addNotice(vo);
		
		return "noticeList.do";
	}

}
