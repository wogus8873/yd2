package co.yedam.notice.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.yedam.common.Command;
import co.yedam.common.Criteria;
import co.yedam.common.PageDTO;
import co.yedam.common.SearchVo;
import co.yedam.notice.service.NoticeService;
import co.yedam.notice.service.impl.NoticeServiceImpl;
import co.yedam.notice.service.impl.NoticeServiceImplMybatis;
import co.yedam.notice.vo.noticeVo;

public class UpdateNotice implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {
		
		//업데이트 구현
		String nid = req.getParameter("num");
		String title = req.getParameter("title");
		String subject = req.getParameter("subject");
		
		NoticeService service = new NoticeServiceImplMybatis();
		noticeVo vo = new noticeVo();
		
		int id = Integer.parseInt(nid);
		
		vo.setNoticeId(id);
		vo.setNoticeTitle(title);
		vo.setNoticeSubject(subject);
		service.updateNotice(vo);
		
		///////////////////////////////////////////////////////////////////////////////
		//페이지정보.
		//		noticeList.java
		String searchCondition = req.getParameter("searchCondition");
		String keyword = req.getParameter("keyword");
		SearchVo svo = new SearchVo();
		svo.setSearchCondition(searchCondition);
		svo.setKeyword(keyword);
		
		String pageNum = req.getParameter("pageNum");
		pageNum = pageNum == null? "1" : pageNum;
		
		int pageNumInt = Integer.parseInt(pageNum);
		
		NoticeService dao = new NoticeServiceImpl();
		int total = dao.noticeListPageingTotalCnt(svo);
		List<noticeVo> list = dao.noticeListPageing(pageNumInt,svo);
		req.setAttribute("noticeList", list);
		
		//페이지기능
		//현재페이지정보, 페이지당 건수 , 전체데이터건수.
		Criteria cri = new Criteria(pageNumInt,10);
		PageDTO dto = new PageDTO(cri,512);
		req.setAttribute("pageDTO", dto);
		
		req.setAttribute("searchvo", svo);
		
		
		return "notice/noticeList.tiles";
	}

}
