package co.yedam.notice.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.yedam.common.Command;
import co.yedam.notice.service.NoticeService;
import co.yedam.notice.service.impl.NoticeServiceImpl;
import co.yedam.notice.service.impl.NoticeServiceImplMybatis;

public class DeleteNotice implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {
		String nId = req.getParameter("num");
		int id = Integer.parseInt(nId);
		
		NoticeService service = new NoticeServiceImplMybatis();
		service.deleteNotice(id);
		
		//파라미터 읽어와서 삭제할 글 번호
		return "noticeList.do";
	}

}
