package co.yedam.notice.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.yedam.common.Command;
import co.yedam.notice.service.NoticeService;
import co.yedam.notice.service.impl.NoticeServiceImplMybatis;

public class DelecteEventJson implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {
		String title = req.getParameter("title");
		
		NoticeService service = new NoticeServiceImplMybatis();
		if(service.deleteEvent(title)>0) {
			return "{\"retCode\":\"Success\"}.ajax";
		}else {
			return "{\"retCode\":\"Fail\"}.ajax";
		}
	}

}
