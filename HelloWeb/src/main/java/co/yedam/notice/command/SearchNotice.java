package co.yedam.notice.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.yedam.common.Command;
import co.yedam.notice.service.NoticeService;
import co.yedam.notice.service.impl.NoticeServiceImpl;
import co.yedam.notice.service.impl.NoticeServiceImplMybatis;
import co.yedam.notice.vo.noticeVo;

public class SearchNotice implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {
		//페이지구현
		String pageNum = req.getParameter("pageNum");
		String searchCondition = req.getParameter("searchCondition");
		String keyword = req.getParameter("keyword");
		
		req.setAttribute("pageNum", pageNum);
		req.setAttribute("searchCondition", searchCondition);
		req.setAttribute("keyword", keyword);
		
		
		String num = req.getParameter("num");
		
		NoticeService service = new NoticeServiceImplMybatis();
		noticeVo vo = service.saerchNotice(Integer.parseInt(num));
		
		req.setAttribute("vo", vo);
		return "notice/searchNotice.tiles";
	}

}
