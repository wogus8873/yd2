package co.yedam.notice.command;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import co.yedam.common.Command;
import co.yedam.notice.service.NoticeService;
import co.yedam.notice.service.impl.NoticeServiceImplMybatis;

public class SearchProductJson implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {
		String code = req.getParameter("code");
		NoticeService service = new NoticeServiceImplMybatis();
		Map<String,Object> prod =service.searchProduct(code);
		ObjectMapper mapper = new ObjectMapper();
		String json = "";
		
		try {
			json = mapper.writeValueAsString(prod);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		return json+".ajax";
	}

}
