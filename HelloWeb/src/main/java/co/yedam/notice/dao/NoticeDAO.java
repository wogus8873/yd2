package co.yedam.notice.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import co.yedam.common.DAO;
import co.yedam.common.SearchVo;
import co.yedam.notice.vo.noticeVo;

public class NoticeDAO {
	Connection conn;
	ResultSet rs;
	PreparedStatement psmt;
	
	private static NoticeDAO instance = new NoticeDAO();
	private NoticeDAO() {
		
	}
	public static NoticeDAO getInstance() {
		return instance;
	}
	//由ъ냼�뒪 蹂��솚
	public void close() {
		try {
			if(conn != null) {
				conn.close();
			}if(rs != null) {
				rs.close();
			}if(psmt != null) {
				psmt.close();
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}
	}
	//chart 데이터
	public Map<String,Integer> chartData(){
		Map<String,Integer> mapData = new HashMap<>();
		conn = DAO.getConn();
		String sql = "select member_name ,count(1)\r\n"
				+ "from notice n  \r\n"
				+ "join member m\r\n"
				+ "on n.notice_writer = m.member_id\r\n"
				+ "group by member_name";
		try {
			psmt = conn.prepareStatement(sql);
			rs = psmt.executeQuery();
			while(rs.next()) {
				mapData.put(rs.getString("member_name"), rs.getInt("count(1)"));
//				mapData.put(rs.getString(1), rs.getInt(2));  위에꺼랑 같은거임 칼럼 순서
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close();
		}
		return mapData;
	}
	//게시글 삭제
	public int deleteNotice(int id) {
		String sql = "DELETE FROM notice WHERE notice_id = ?";
		conn = DAO.getConn();
		try {
			psmt = conn.prepareStatement(sql);
			psmt.setInt(1, id);
			int r = psmt.executeUpdate();
			
			return r;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close();
		}
		return 0;
	}
	//게시글 추가
	public int addNotice(noticeVo vo) {
		String sql = "insert into notice (notice_id , notice_writer,notice_title,notice_subject,notice_date)\r\n"
				+ "values (notice_seq.nextval, ? , ? , ? , sysdate)";
		conn = DAO.getConn();
		try {
			psmt = conn.prepareStatement(sql);
			psmt.setString(1, vo.getNoticeWriter());
			psmt.setString(2, vo.getNoticeTitle());
			psmt.setString(3, vo.getNoticeSubject());
			
			int r = psmt.executeUpdate();
			
			return r;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close();
		}
		return 0;
	}
	
	//게시글수정
	public int updateNotice(noticeVo vo) { //title subject 변경
		String sql = "update notice set notice_title=? , notice_subject=? where notice_id= ?";
		conn = DAO.getConn();
		try {
			psmt = conn.prepareStatement(sql);
			psmt.setString(1, vo.getNoticeTitle());
			psmt.setString(2, vo.getNoticeSubject());
			psmt.setInt(3, vo.getNoticeId());
			int r = psmt.executeUpdate();
			return r;
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close();
		}
		return 0;
	}
	//조회수증가
	public void addHitCount(int noticeId) {
		String sql = "update notice set hit_count = hit_count+1 where notice_id = ?";
		conn = DAO.getConn();
		try {
			psmt = conn.prepareStatement(sql);
			psmt.setInt(1, noticeId);
			int r = psmt.executeUpdate();
			if(r > 0){
				System.out.println("변경완료");
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close();
		}
	}
	
	//상세정보조회
	public noticeVo searchNotice(int noticeId) {
		String sql = "select * from notice where notice_id = ?";
		conn = DAO.getConn();
		try {
			psmt = conn.prepareStatement(sql);
			psmt.setInt(1, noticeId);
			rs = psmt.executeQuery();
			if(rs.next()) {
				noticeVo vo = new noticeVo();
				vo.setNoticeId(rs.getInt("notice_id"));
				vo.setNoticeWriter(rs.getString("notice_writer"));
				vo.setNoticeTitle(rs.getString("notice_title"));
				vo.setNoticeSubject(rs.getString("notice_subject"));
				vo.setHitCount(rs.getInt("hit_count"));
				vo.setNoticeDate(rs.getDate("notice_date"));
				return vo;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close();
		}
		return null;
	}
	
	//건수 가져오는 메소드
	public int noticeListTotalCnt(SearchVo svo) {
		String sql = "select count(1) from notice ";
		if(svo.getSearchCondition() != null && svo.getSearchCondition().equals("writer")) {
			sql += "where notice_writer like '%'||?||'%'";
		}else if(svo.getSearchCondition() != null &&svo.getSearchCondition().equals("title")) {
			sql += "where notice_title like '%'||?||'%'";
		}else if(svo.getSearchCondition() != null &&svo.getSearchCondition().equals("subject")) {
			sql += "where notice_subject like '%'||?||'%'";
		}
		
		conn = DAO.getConn();
		try {
			psmt = conn.prepareStatement(sql);
			//searchCondition 
			if(svo.getSearchCondition() != null && !svo.getSearchCondition().equals("")) {
				psmt.setString(1, svo.getKeyword());
			}
			rs = psmt.executeQuery();
			
			if(rs.next()) {
				return rs.getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close();
		}
		return 0;
	}
	
	public List<noticeVo> noticeListPageing(int page,SearchVo svo){
		List<noticeVo> list = new ArrayList<>();
		String sql = "select * from\r\n"
				+ "(select rownum rn, a.*\r\n"
				+ "from(select * from notice where 1=1 ";
		
				if(svo.getSearchCondition() != null && svo.getSearchCondition().equals("writer")) {
					sql += " and notice_writer like '%'||?||'%'";
				}else if(svo.getSearchCondition() != null && svo.getSearchCondition().equals("title")) {
					sql += " and notice_title like '%'||?||'%'";
				}else if(svo.getSearchCondition() != null && svo.getSearchCondition().equals("subject")) {
					sql += " and  notice_subject like '%'||?||'%'";
				}
				
				sql += " order by notice_id desc) a\r\n"
				+ " where rownum <= (?*10)) b\r\n"
				+ " where b.rn >= (?*10-9)";
	
			
		conn = DAO.getConn();
		try {
			psmt = conn.prepareStatement(sql);
			int row = 1;
			if(svo.getSearchCondition() != null && !svo.getSearchCondition().equals("") ) {
				psmt.setString(row++, svo.getKeyword());
			}
			psmt.setInt(row++, page);
			psmt.setInt(row++, page);
			rs = psmt.executeQuery();
			while(rs.next()) {
				noticeVo vo = new noticeVo();
				vo.setNoticeId(rs.getInt("notice_id"));
				vo.setNoticeWriter(rs.getString("notice_writer"));
				vo.setNoticeTitle(rs.getString("notice_title"));
				vo.setNoticeSubject(rs.getString("notice_subject"));
				vo.setNoticeDate(rs.getDate("notice_date"));
				vo.setHitCount(rs.getInt("hit_count"));
				vo.setAttachFile(rs.getString("attach_file"));
				list.add(vo);
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}finally {
			close();
		}
		return list;
	}
	
	
	
}
