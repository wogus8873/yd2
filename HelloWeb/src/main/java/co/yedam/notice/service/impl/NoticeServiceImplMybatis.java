package co.yedam.notice.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import co.yedam.common.Criteria;
import co.yedam.common.DataSource;
import co.yedam.common.SearchVo;
import co.yedam.notice.mapper.NoticeMapper;
import co.yedam.notice.service.NoticeService;
import co.yedam.notice.vo.noticeVo;

public class NoticeServiceImplMybatis implements NoticeService {
	
	SqlSession session = DataSource.getInstance().openSession(true);
	NoticeMapper mapper = session.getMapper(NoticeMapper.class);
	
	@Override
	public List<noticeVo> noticeList() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<noticeVo> noticeListPageing(int pageNum, SearchVo svo) {
		// TODO Auto-generated method stub
		//mybatis에 parameter 로 넘기기 위해 Criteria 활용 
				Criteria cri = new Criteria(pageNum,10);
				cri.setSearchCondition(svo.getSearchCondition());
				cri.setKeyword(svo.getKeyword());
		return mapper.noticeListPaging(cri);
	}

	@Override
	public int noticeListPageingTotalCnt(SearchVo svo) {
		// TODO Auto-generated method stub
		return mapper.noticeListPagingTotalCnt(svo);
	}

	@Override
	public noticeVo saerchNotice(int noticeId) {
		mapper.addHitCount(noticeId);
		return mapper.searchNotice(noticeId);
	}

	@Override
	public int updateNotice(noticeVo vo) {
		// TODO Auto-generated method stub
		return mapper.updateNotice(vo);
	}

	@Override
	public int addNotice(noticeVo vo) {
		// TODO Auto-generated method stub
		return mapper.insertNotice(vo);
	}

	@Override
	public int deleteNotice(int noticeId) {
		return mapper.deleteNotice(noticeId);
	}

	@Override
	public Map<String, Integer> charData() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	//forEach연습
	public List<noticeVo> forEachTest(List<noticeVo> list){
		return mapper.forEachTest(list);
	}
	public int addCenterInfo(Map<String,Object> map) {
		return mapper.addCenterInfo(map);
	}
	public int insertCenterInfo(List<Map<String,Object>> list) {
		return mapper.insertCenterInfo(list);
	}
	
	//이벤트
	@Override
	public List<Map<String, Object>> allEvents() {
		// TODO Auto-generated method stub
		return mapper.allEvents();
	}

	@Override
	public int addEvent(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return mapper.addEvent(map);
	}

	@Override
	public int deleteEvent(String title) {
		// TODO Auto-generated method stub
		return mapper.deleteEvent(title);
	}

	@Override
	public List<Map<String, Object>> productList() {
		// TODO Auto-generated method stub
		return mapper.productList();
	}

	@Override
	public Map<String, Object> searchProduct(String code) {
		// TODO Auto-generated method stub
		return mapper.searchProduct(code);
	}

	@Override
	public List<Map<String, Object>> bestProductList() {
		// TODO Auto-generated method stub
		return mapper.bestProductList();
	}

	
	
}
