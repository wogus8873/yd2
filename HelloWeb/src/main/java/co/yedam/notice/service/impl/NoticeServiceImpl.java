package co.yedam.notice.service.impl;

import java.util.List;
import java.util.Map;

import co.yedam.common.SearchVo;
import co.yedam.notice.dao.NoticeDAO;
import co.yedam.notice.service.NoticeService;
import co.yedam.notice.vo.noticeVo;

public class NoticeServiceImpl implements NoticeService{
	//jdbc �궗�슜泥섎━.
	NoticeDAO dao = NoticeDAO.getInstance();
	
	@Override
	public List<noticeVo> noticeList() {
		
		return dao.noticeListPageing(0, null);
	}

	@Override
	public List<noticeVo> noticeListPageing(int pageNum, SearchVo svo) {
		
		return dao.noticeListPageing(pageNum,svo);
	}

	@Override
	public int noticeListPageingTotalCnt(SearchVo svo) {
		return dao.noticeListTotalCnt(svo);
	}

	@Override
	public noticeVo saerchNotice(int noticeId) {
		dao.addHitCount(noticeId);
		return dao.searchNotice(noticeId);
	}

	@Override
	public int updateNotice(noticeVo vo) {
		
		return dao.updateNotice(vo);
	}

	@Override
	public int addNotice(noticeVo vo) {
		return dao.addNotice(vo);
	}

	@Override
	public int deleteNotice(int noticeId) {
		return dao.deleteNotice(noticeId);
	}

	@Override
	public Map<String, Integer> charData() {
		
		return dao.chartData();
	}

	@Override
	public List<Map<String, Object>> allEvents() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int addEvent(Map<String, Object> map) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int deleteEvent(String title) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public List<Map<String, Object>> productList() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Map<String, Object> searchProduct(String code) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Map<String, Object>> bestProductList() {
		// TODO Auto-generated method stub
		return null;
	}

	
	
	
}
