package co.yedam.notice.service;

import java.util.List;
import java.util.Map;

import co.yedam.common.SearchVo;
import co.yedam.notice.vo.noticeVo;

public interface NoticeService {
	public List<noticeVo> noticeList();
	
	//page에 따른 목록
	public List<noticeVo> noticeListPageing(int pageNum,SearchVo svo);
	//page 목록 계산을 위한 건수
	public int noticeListPageingTotalCnt(SearchVo svo);
	
	//단건조회. 글번호 -> 글전체정보 반환
	public noticeVo saerchNotice(int noticeId);
	//글정보수정
	public int updateNotice(noticeVo vo);
	//게시글추가
	public int addNotice(noticeVo vo);
	//글삭제
	public int deleteNotice(int noticeId);
	
	//차트용 데이터
	public Map<String,Integer> charData();
	
	//에빈트 데이터 
	public List<Map<String,Object>> allEvents();
	//이벤트등록
	public int addEvent(Map<String,Object> map);
	//이벤트삭제
	public int deleteEvent(String title);
	
	//상품리스트
	public List<Map<String,Object>> productList();
	//상품단건
	public Map<String,Object> searchProduct(String code);
	//베스트 상품
	public List<Map<String,Object>> bestProductList();

	
}
