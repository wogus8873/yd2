package co.yedam.notice.mapper;

import java.util.List;
import java.util.Map;

import co.yedam.common.Criteria;
import co.yedam.common.SearchVo;
import co.yedam.notice.vo.noticeVo;

public interface NoticeMapper {
	//멤버등록
	public int insertNotice(noticeVo vo);
	//멤버조회
	public noticeVo searchNotice(int noticeId);
	//조회수
	public int addHitCount(int noticeId);
	//수정
	public int updateNotice(noticeVo vo);
	//삭제
	public int deleteNotice(int id);
	//paging 처리를 위한 전체 건수 계산
	public int noticeListPagingTotalCnt(SearchVo svo);
	public List<noticeVo> noticeListPaging(Criteria cri);
	//forEachTest
	public List<noticeVo> forEachTest(List<noticeVo> list);
	public int addCenterInfo(Map<String,Object> map);
	public int insertCenterInfo(List<Map<String,Object>> list);
	
	//이벤트 데이터 처리
	public List<Map<String, Object>> allEvents();
	//이벤트 추가
	public int addEvent(Map<String,Object> map);
	//이벤트 삭제
	public int deleteEvent(String title);
	
	//상품리스트
	public List<Map<String, Object>> productList();
	//상품단건
	public Map<String, Object> searchProduct(String code);
	//베스트
	public List<Map<String, Object>> bestProductList();
	
}
