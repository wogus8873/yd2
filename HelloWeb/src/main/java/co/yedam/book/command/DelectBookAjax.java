package co.yedam.book.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.yedam.book.service.BookService;
import co.yedam.book.service.impl.BookServiceImplMybatis;
import co.yedam.common.Command;

public class DelectBookAjax implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {
		String code = req.getParameter("code");
		BookService service = new BookServiceImplMybatis();
		int r = service.deleteMember(code);
		String json = "";
		if(r>0) {
			json = "{\"retCode\":\"Success\"}";
		}else {
			json = "{\"retCode\":\"Fail\"}";
		}
		
		return json+".ajax";
	}
	

}
