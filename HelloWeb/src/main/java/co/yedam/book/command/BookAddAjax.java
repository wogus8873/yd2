package co.yedam.book.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.yedam.book.service.BookService;
import co.yedam.book.service.impl.BookServiceImplMybatis;
import co.yedam.book.vo.BookVo;
import co.yedam.common.Command;

public class BookAddAjax implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {
		String code = req.getParameter("bookCode");
		String title = req.getParameter("bookTitle");
		String author = req.getParameter("bookAuthor");
		String press = req.getParameter("bookPress");
		int price = Integer.parseInt(req.getParameter("bookPrice"));
		
		
		BookVo vo = new BookVo();
		vo.setBookCode(code);
		vo.setBookTitle(title);
		vo.setBookAuthor(author);
		vo.setBookPress(press);
		vo.setBookPrice(price);
		
		BookService service = new BookServiceImplMybatis();
		int r = service.addBook(vo);
		String json= "";
		if(r>0) {
			json = "{\"retCode\":\"Success\"}";
		}else {
			json = "{\"retCode\":\"Fail\"}";
		}
		
		return json+".ajax";
	}

}
