package co.yedam.book.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.yedam.book.dao.BookDAO;
import co.yedam.book.service.BookService;
import co.yedam.book.service.impl.BookServiceImpl;
import co.yedam.book.service.impl.BookServiceImplMybatis;
import co.yedam.book.vo.BookVo;
import co.yedam.common.Command;

public class addBook implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {
		String code = req.getParameter("bookCode");
		String title = req.getParameter("bookTitle");
		String author = req.getParameter("bookAuthor");
		String press = req.getParameter("bookPress");
		int price = Integer.parseInt(req.getParameter("bookPrice"));
		
		BookVo vo = new BookVo();
		vo.setBookCode(code);
		vo.setBookTitle(title);
		vo.setBookAuthor(author);
		vo.setBookPress(press);
		vo.setBookPrice(price);
		
		BookService service = new BookServiceImplMybatis();
		service.addBook(vo);
		
		return "bookList.do";
	}

}
