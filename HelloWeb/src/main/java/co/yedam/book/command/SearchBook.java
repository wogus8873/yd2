package co.yedam.book.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.yedam.book.service.BookService;
import co.yedam.book.service.impl.BookServiceImpl;
import co.yedam.book.service.impl.BookServiceImplMybatis;
import co.yedam.book.vo.BookVo;
import co.yedam.common.Command;

public class SearchBook implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {
		String code = req.getParameter("code");
		
		BookService service = new BookServiceImplMybatis();
		BookVo vo = service.searchBook(code);
		
		req.setAttribute("vo", vo);
		return "book/searchBook.tiles";
	}

}
