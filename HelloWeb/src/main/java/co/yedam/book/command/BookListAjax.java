package co.yedam.book.command;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import co.yedam.book.service.BookService;
import co.yedam.book.service.impl.BookServiceImplMybatis;
import co.yedam.common.Command;

public class BookListAjax implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {
		
		BookService service = new BookServiceImplMybatis();
		List<Map<String,Object>> list =service.bookListAjax();
		
		ObjectMapper mapper = new ObjectMapper();
		String json = "";
		
		try {
			json = mapper.writeValueAsString(list);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		
		return json+".ajax";
	}

}
