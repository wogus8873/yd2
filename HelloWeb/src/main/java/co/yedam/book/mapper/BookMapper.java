package co.yedam.book.mapper;

import java.util.List;
import java.util.Map;

import co.yedam.book.vo.BookVo;

public interface BookMapper {
	//책추가
	public int insertBook(BookVo vo);
	//책리스트
	public List<BookVo> bookList();
	//책조회
	public BookVo searchBook(String code);
	
	//시험
	public List<Map<String, Object>> bookListAjax();
	
	public int deleteMember(String code);
}
