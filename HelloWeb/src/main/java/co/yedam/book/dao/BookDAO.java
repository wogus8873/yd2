package co.yedam.book.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import co.yedam.book.vo.BookVo;
import co.yedam.common.DAO;

public class BookDAO {
	Connection conn;
	ResultSet rs;
	PreparedStatement psmt;
	
	
	private static BookDAO instance = new BookDAO();
	private BookDAO() {
		
	}
	public static BookDAO getInstance() {
		return instance;
	}
	
	public void close() {
		try {
			if(conn != null) {
				conn.close();
			}if(rs != null) {
				rs.close();
			}if(psmt != null) {
				psmt.close();
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	//책 추가
	public int addBook(BookVo vo) {
		String sql = "insert into tbl_book\r\n"
				+ "values(?,?,?,?,?)";
		conn = DAO.getConn();
		try {
			psmt = conn.prepareStatement(sql);
			psmt.setString(1, vo.getBookCode());
			psmt.setString(2, vo.getBookTitle());
			psmt.setString(3, vo.getBookAuthor());
			psmt.setString(4, vo.getBookPress());
			psmt.setInt(5, vo.getBookPrice());
			
			int r = psmt.executeUpdate();
			return r;
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close();
		}
		return 0;
	}
	//책 조회
	public List<BookVo> bookList(){
		List<BookVo> list = new ArrayList<>();
		String sql = "select * from tbl_book order by book_code";
		conn = DAO.getConn();
		try {
			psmt = conn.prepareStatement(sql);
			rs = psmt.executeQuery();
			
			while(rs.next()) {
				BookVo vo = new BookVo();
				vo.setBookCode(rs.getString("book_code"));
				vo.setBookTitle(rs.getString("book_title"));
				vo.setBookAuthor(rs.getString("book_author"));
				vo.setBookPress(rs.getString("book_press"));
				vo.setBookPrice(rs.getInt("book_price"));
				list.add(vo);
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}finally {
			close();
		}
		return list;
	}
	
	//책 상세조회
	public BookVo searchBook(String code) {
		BookVo vo = new BookVo();
		String sql = "select * from tbl_book where book_code = ?";
		conn = DAO.getConn();
		try {
			psmt = conn.prepareStatement(sql);
			psmt.setString(1, code);
			
			rs = psmt.executeQuery();
			
			if(rs.next()) {
				vo.setBookCode(rs.getString("book_code"));
				vo.setBookTitle(rs.getString("book_title"));
				vo.setBookAuthor(rs.getString("book_author"));
				vo.setBookPress(rs.getString("book_press"));
				vo.setBookPrice(rs.getInt("book_price"));
				return vo;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			close();
		}
		return null;
	}
	
}
