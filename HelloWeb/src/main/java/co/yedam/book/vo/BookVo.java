package co.yedam.book.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class BookVo {
//	book_code varchar2(10) primary key,
//	book_title varchar2(100) not null,
//	book_author varchar2(100) not null,
//	book_press varchar2(100) not null,
//	book_price number default 0
	private String bookCode;
	private String bookTitle;
	private String bookAuthor;
	private String bookPress;
	private int bookPrice;
}
