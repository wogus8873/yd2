package co.yedam.book.service.impl;

import java.util.List;
import java.util.Map;

import co.yedam.book.dao.BookDAO;
import co.yedam.book.service.BookService;
import co.yedam.book.vo.BookVo;

public class BookServiceImpl implements BookService {

	@Override
	public int addBook(BookVo vo) {
		BookDAO dao = BookDAO.getInstance();
		return dao.addBook(vo);
	}

	@Override
	public List<BookVo> bookList() {
		BookDAO dao = BookDAO.getInstance();
		return dao.bookList();
	}

	@Override
	public BookVo searchBook(String code) {
		BookDAO dao = BookDAO.getInstance();
		return dao.searchBook(code);
	}

	@Override
	public List<Map<String, Object>> bookListAjax() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int deleteMember(String code) {
		// TODO Auto-generated method stub
		return 0;
	}

}
