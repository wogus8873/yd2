package co.yedam.book.service.impl;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;

import co.yedam.book.mapper.BookMapper;
import co.yedam.book.service.BookService;
import co.yedam.book.vo.BookVo;
import co.yedam.common.DataSource;

public class BookServiceImplMybatis implements BookService {
	
	SqlSession session = DataSource.getInstance().openSession(true);
	BookMapper mapper = session.getMapper(BookMapper.class);
	
	@Override
	public int addBook(BookVo vo) {
		// TODO Auto-generated method stub
		return mapper.insertBook(vo);
	}

	@Override
	public List<BookVo> bookList() {
		// TODO Auto-generated method stub
		return mapper.bookList();
	}

	@Override
	public BookVo searchBook(String code) {
		// TODO Auto-generated method stub
		return mapper.searchBook(code);
	}

	@Override
	public List<Map<String, Object>> bookListAjax() {
		// TODO Auto-generated method stub
		return mapper.bookListAjax();
	}

	@Override
	public int deleteMember(String code) {
		// TODO Auto-generated method stub
		return mapper.deleteMember(code);
	}

}
