package co.yedam.book.service;

import java.util.List;
import java.util.Map;

import co.yedam.book.vo.BookVo;

public interface BookService {
	public int addBook(BookVo vo);
	
	public List<BookVo> bookList();
	
	public BookVo searchBook(String code);
	
	public List<Map<String,Object>> bookListAjax();
	
	public int deleteMember(String code);
}
