package co.yedam.member.service;

import java.util.List;

import co.yedam.member.vo.MemberVo;

public interface MemberService {
	public MemberVo loginCheck(String id, String pw);
	
	//멤버등록 
	public int addMember(MemberVo vo); 
	//멤버 전체조회
	public List<MemberVo> MemberList();
	//삭제
	public int deleteMember(String id);
	//멤버수정
	public int updateMember(MemberVo vo);
	//멤버조회//안만들었음
	public MemberVo searchMember(String id);
	
}
