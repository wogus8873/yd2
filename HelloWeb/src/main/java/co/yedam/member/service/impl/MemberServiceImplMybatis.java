package co.yedam.member.service.impl;

import java.util.List;

import org.apache.ibatis.session.SqlSession;

import co.yedam.common.DataSource;
import co.yedam.member.service.MemberService;
import co.yedam.member.vo.MemberVo;

public class MemberServiceImplMybatis implements MemberService {
	//mybatis 활용 
	//DataSource.getInstance() => SqlSessionFactory 반환
	SqlSession session = DataSource.getInstance().openSession(true);
	
	
	@Override
	public MemberVo loginCheck(String id, String pw) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int addMember(MemberVo vo) {
		return session.insert("co.yedam.member.mapper.MemberMapper.addMember",vo);
	}

	@Override
	public List<MemberVo> MemberList() {
		return session.selectList("co.yedam.member.mapper.MemberMapper.memberList");
	}

	@Override
	public int deleteMember(String id) {
		// TODO Auto-generated method stub
		return session.delete("co.yedam.member.mapper.MemberMapper.deleteMember",id);
	}
//deleteMember
	@Override
	public int updateMember(MemberVo vo) {
		// TODO Auto-generated method stub
		return session.update("co.yedam.member.mapper.MemberMapper.updateMember",vo);
	}

	@Override
	public MemberVo searchMember(String id) {
		return session.selectOne("co.yedam.member.mapper.MemberMapper.searchMember",id);
	}

}
