package co.yedam.member.service.impl;

import java.util.List;

import co.yedam.member.dao.MemberDAO;
import co.yedam.member.service.MemberService;
import co.yedam.member.vo.MemberVo;

public class MemberServiceImpl implements MemberService {

	@Override
	public MemberVo loginCheck(String id, String pw) {
		MemberDAO dao = MemberDAO.getInstance();
		return dao.loginCheck(id, pw);
	}

	@Override
	public int addMember(MemberVo vo) {
		MemberDAO dao = MemberDAO.getInstance();
		return dao.addMember(vo);
	}

	@Override
	public List<MemberVo> MemberList() {
		MemberDAO dao = MemberDAO.getInstance();
		return dao.MemberList();
	}

	@Override
	public int deleteMember(String id) {
		// TODO Auto-generated method stub
		MemberDAO dao = MemberDAO.getInstance();
		return dao.deleteMember(id);
	}

	@Override
	public int updateMember(MemberVo vo) {
		// TODO Auto-generated method stub
		MemberDAO dao = MemberDAO.getInstance();
		
		return dao.updateMember(vo);
	}

	@Override
	public MemberVo searchMember(String id) {
		return null;
	}

	
	
}
