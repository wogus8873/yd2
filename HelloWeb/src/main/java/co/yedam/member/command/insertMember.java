package co.yedam.member.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.yedam.common.Command;
import co.yedam.member.service.MemberService;
import co.yedam.member.service.impl.MemberServiceImpl;
import co.yedam.member.vo.MemberVo;

public class insertMember implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {

		String id = req.getParameter("memberId");
		String pw = req.getParameter("memberPw");
		String name = req.getParameter("memberName");
		String ph = req.getParameter("memberPhone");
		String ad = req.getParameter("memberAddr");
		MemberVo vo = new MemberVo();
		vo.setMemberId(id);
		vo.setMemberPw(pw);
		vo.setMemberName(name);
		vo.setMemberPhone(ph);
		vo.setMemberAddr(ad);
		
		MemberService service = new MemberServiceImpl();
		service.addMember(vo);
		
		return "memberList.do";
	}

}
