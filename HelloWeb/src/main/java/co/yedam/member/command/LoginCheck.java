package co.yedam.member.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import co.yedam.common.Command;
import co.yedam.member.service.MemberService;
import co.yedam.member.service.impl.MemberServiceImpl;
import co.yedam.member.vo.MemberVo;

public class LoginCheck implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {
		// TODO Auto-generated method stub
		HttpSession session = req.getSession(true); // session 정보
		String id = req.getParameter("id");
		String pw = req.getParameter("pw");
		
		MemberService service = new MemberServiceImpl();
		MemberVo vo = service.loginCheck(id, pw);
		String message = "";
		
		if(vo == null) {
			message = "아이디와 비밀번호 확인하세요";
			req.setAttribute("msg", message);
			return "member/loginForm.tiles";
		}else {
			message = "환영합니다";
			session.setAttribute("msg", message);
			session.setAttribute("id", vo.getMemberId());
			session.setAttribute("name", vo.getMemberName());
			session.setAttribute("grade", vo.getMemberGrade());
			
			return "noticeList.do";
		}
	}

}
