package co.yedam.member.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import co.yedam.common.Command;
import co.yedam.member.service.MemberService;
import co.yedam.member.service.impl.MemberServiceImpl;
import co.yedam.member.service.impl.MemberServiceImplMybatis;
import co.yedam.member.vo.MemberVo;

public class MemberListAjaxJackson implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {
		MemberService service = new MemberServiceImplMybatis();
		List<MemberVo> list = service.MemberList();
		
		//jackson 
		ObjectMapper mapper = new ObjectMapper();
		try {
			String json = mapper.writeValueAsString(list);
			return json+".ajax";
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		return null;
	}

}
