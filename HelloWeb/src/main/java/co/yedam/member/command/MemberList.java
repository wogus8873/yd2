package co.yedam.member.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.yedam.common.Command;
import co.yedam.member.dao.MemberDAO;
import co.yedam.member.vo.MemberVo;

public class MemberList implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {
		MemberDAO dao = MemberDAO.getInstance();
		
		List<MemberVo> list = dao.MemberList(); 
		req.setAttribute("memberList", list);
		return "member/memberList.tiles";
	}

}
