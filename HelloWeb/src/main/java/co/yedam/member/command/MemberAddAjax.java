package co.yedam.member.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.yedam.common.Command;
import co.yedam.member.service.MemberService;
import co.yedam.member.service.impl.MemberServiceImpl;
import co.yedam.member.service.impl.MemberServiceImplMybatis;
import co.yedam.member.vo.MemberVo;

public class MemberAddAjax implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {
		// TODO Auto-generated method stub
		String id = req.getParameter("id");
		String na = req.getParameter("name");
		String pw = req.getParameter("pass");
		String ad = req.getParameter("addr");
		String ph = req.getParameter("phone");
		
		MemberVo vo = new MemberVo();
		vo.setMemberId(id);
		vo.setMemberName(na);
		vo.setMemberPw(pw);
		vo.setMemberPhone(ph);
		vo.setMemberAddr(ad);
		
		MemberService service = new MemberServiceImplMybatis();
		int r = service.addMember(vo);
		String json= "";
		if(r>0) {
			json = "{\"retCode\":\"Success\"}";
		}else {
			json = "{\"retCode\":\"Fail\"}";
		}
		
		return json+".ajax";
	}

}
