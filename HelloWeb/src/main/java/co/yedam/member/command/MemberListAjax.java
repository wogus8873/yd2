package co.yedam.member.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.yedam.common.Command;
import co.yedam.member.service.MemberService;
import co.yedam.member.service.impl.MemberServiceImpl;
import co.yedam.member.vo.MemberVo;

public class MemberListAjax implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {
		//{"id":"hong","age":20, "phone":"010-1111-2222"}
		MemberService service = new MemberServiceImpl();
		String json = "{\"id\":\"hong\",\"age\":20, \"phone\":\"010-1111-2222\"}";
		List<MemberVo> list = service.MemberList();
		
		json ="[";
		for(int i=0; i<list.size(); i++) {
			json += "{\"memberId\":\""+list.get(i).getMemberId()
					+"\",\"memberName\":\""+list.get(i).getMemberName()
					+"\",\"memberAddr\":\""+list.get(i).getMemberAddr()
					+"\"}";
			if(i != list.size()-1) {
				json += ",";
			}
		}
		json +="]";
		
		return json + ".ajax";
	}

}
