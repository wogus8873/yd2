package co.yedam.member.vo;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class MemberVo {
	private String memberId;
	private String memberPw;
	private String memberPhone;
	private String memberAddr;
	private String memberGrade;
	private String image;
	private String memberName;
}
