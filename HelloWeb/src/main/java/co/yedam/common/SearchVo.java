package co.yedam.common;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class SearchVo {
	private String searchCondition;
	private String keyword;
}
