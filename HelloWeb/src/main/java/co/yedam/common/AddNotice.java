package co.yedam.common;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.yedam.notice.service.NoticeService;
import co.yedam.notice.service.impl.NoticeServiceImpl;
import co.yedam.notice.vo.noticeVo;

public class AddNotice implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {
	
		return "notice/addNotice.tiles";
	}

}
