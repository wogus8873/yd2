package co.yedam.common;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import co.yedam.notice.service.impl.NoticeServiceImplMybatis;

public class AddCenterJson implements Command {

	@Override
	public String exec(HttpServletRequest req, HttpServletResponse resp) {
		int result=0;
		try {
			ServletInputStream sis = req.getInputStream(); //byte형태 
			byte[] bytes = sis.readAllBytes();
			String json = new String(bytes); //String json = "test";
			System.out.println(json);
			
			//json => object : JSON.parse
			JSONParser parser = new JSONParser();
			Object obj = parser.parse(json);
			
			//[{},{},{},{}]
			ArrayList<Map<String,Object>> list = (ArrayList<Map<String,Object>>) obj;
			//콘솔에 출력해서 보려고
			for(Map<String,Object> map : list) {
				Set<String> keys = map.keySet();
				for(String key : keys) {
					System.out.println(key+":"+map.get(key));
				}
			}
			//메소드 호출
			NoticeServiceImplMybatis ibatis = new NoticeServiceImplMybatis();
			result = ibatis.insertCenterInfo(list);
			
		} catch (IOException | ParseException e) {
			e.printStackTrace();
		}
		return "{retCode: "+result+"}.ajax";
	}

}
