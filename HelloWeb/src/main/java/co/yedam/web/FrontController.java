package co.yedam.web;

import java.io.IOException;
import java.util.HashMap;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import co.yedam.book.command.BookAddAjax;
import co.yedam.book.command.BookInfoAjax;
import co.yedam.book.command.BookList;
import co.yedam.book.command.BookListAjax;
import co.yedam.book.command.DelectBookAjax;
import co.yedam.book.command.SearchBook;
import co.yedam.book.command.addBook;
import co.yedam.book.command.addBookForm;
import co.yedam.common.AddCenterInfo;
import co.yedam.common.AddCenterJson;
import co.yedam.common.AddNotice;
import co.yedam.common.Command;
import co.yedam.common.MainDo;
import co.yedam.member.command.AddMember;
import co.yedam.member.command.AjaxForm;
import co.yedam.member.command.LoginCheck;
import co.yedam.member.command.LoginFrom;
import co.yedam.member.command.Logout;
import co.yedam.member.command.MemberAddAjax;
import co.yedam.member.command.MemberDeleteAjax;
import co.yedam.member.command.MemberList;
import co.yedam.member.command.MemberListAjax;
import co.yedam.member.command.MemberListAjaxJackson;
import co.yedam.member.command.MemberModAjax;
import co.yedam.member.command.insertMember;
import co.yedam.notice.command.AddEventJson;
import co.yedam.notice.command.AllEventsJson;
import co.yedam.notice.command.BestProductJson;
import co.yedam.notice.command.ChartDo;
import co.yedam.notice.command.DelecteEventJson;
import co.yedam.notice.command.DeleteNotice;
import co.yedam.notice.command.FullCalendarDo;
import co.yedam.notice.command.InsertNotice;
import co.yedam.notice.command.NoticeList;
import co.yedam.notice.command.ProductListJson;
import co.yedam.notice.command.SearchNotice;
import co.yedam.notice.command.SearchProductJson;
import co.yedam.notice.command.UpdateNotice;

@WebServlet("*.do")
public class FrontController extends HttpServlet {
	HashMap<String , Command> map;
	
	public FrontController() {
		map = new HashMap<>();
	}
	
	@Override
	public void init() throws ServletException {
		//글관련
		map.put("/main.do", new MainDo());
		map.put("/noticeList.do", new NoticeList());
		map.put("/searchNotice.do", new SearchNotice());
		map.put("/updateNotice.do", new UpdateNotice());
		map.put("/addNotice.do", new AddNotice());
		map.put("/insertNotice.do", new InsertNotice());
		map.put("/deleteNotice.do", new DeleteNotice());
		//회원관련
		map.put("/loginForm.do", new LoginFrom());
		map.put("/loginCheck.do", new LoginCheck());
		map.put("/logout.do", new Logout());
		map.put("/addMemberFrom.do", new AddMember());
		map.put("/memberList.do", new MemberList());
		map.put("/addMember.do", new insertMember());
		//책관련
		map.put("/addBookForm.do", new addBookForm());
		map.put("/addBook.do", new addBook());
		map.put("/bookList.do", new BookList());
		map.put("/searchBook.do", new SearchBook());
		//시험
		map.put("/bookInfoAjax.do", new BookInfoAjax());
		map.put("/bookListAjax.do", new BookListAjax());
		map.put("/bookAddAjax.do", new BookAddAjax());
		map.put("/deleteBook.do", new DelectBookAjax());
		
		
		//ajax
		map.put("/ajaxMember.do", new AjaxForm());		
		map.put("/memberListAjax.do", new MemberListAjax());
		map.put("/memberListAjaxJackson.do", new MemberListAjaxJackson());
		map.put("/deleteMemberAjax.do", new MemberDeleteAjax());
		map.put("/addMemberAjax.do", new MemberAddAjax());
		map.put("/modMemberAjax.do", new MemberModAjax());
		
		//chart 관련
		map.put("/chart.do", new ChartDo());
		//이벤트 정보
		map.put("/allEventJson.do", new AllEventsJson());
		map.put("/addEvent.do", new AddEventJson());
		map.put("/deleteEvent.do", new DelecteEventJson());
		map.put("/fullCalendar.do", new FullCalendarDo());
		
		//여려건의 데이터를 center_info 등록
		map.put("/addCenterInfo.do", new AddCenterInfo());
		map.put("/addCenterJson.do", new AddCenterJson());
		
		//상품
		map.put("/productList.do", new ProductListJson());
		map.put("/searchProduct.do", new SearchProductJson());
		map.put("/bestProduct.do", new BestProductJson());
		
	}
	
	
	@Override
	protected void service(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		req.setCharacterEncoding("utf-8");
		String uri = req.getRequestURI();//HelloWeb/hello.do
		String context = req.getContextPath();//HelloWeb
		String page = uri.substring(context.length());
		System.out.println(">"+page);
		
		
		Command command = map.get(page);
		String viewPage = command.exec(req, resp); //main/main.tiles
		
		//.do 요청 들어오면 리다이렉트 경로 안 써도된다?
		//삭제하는경우 바로 갱신되게 하는게 리다이렉트
		if(viewPage.endsWith(".do")) {
			resp.sendRedirect(viewPage);
		//forward는 삭제하고 새로고침해야 갱신 차이점을 아시겠나요?
		}else if(viewPage.endsWith(".tiles")) {
			RequestDispatcher rd = req.getRequestDispatcher(viewPage); 
			rd.forward(req, resp);
		}else if(viewPage.endsWith(".ajax")) {//{"id:"hong","age":20}.ajax
			resp.setContentType("text/json;charset=utf-8");
			resp.getWriter().print(viewPage.substring(0,viewPage.length()-5)); //.ajax 는 필요없어서 빼는것 json형태의{"id:"hong","age":20} 만 출력
			
		}
		
		
		
	}
}
